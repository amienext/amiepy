import amie_core, amie_basis
import numpy as np
import os
import matplotlib.pyplot as pp

cov_table_dir = os.path.join(amie_core.tables_dir,'covariance')

class PriorModelErrorCovariance(object):
	"""
	This class is a generic representation of 
	the covariance of the background model,
	which is NOT directly computed nessecarily,
	and is generally modeled using EOFs (e.g. Matsuo 2002)
	and the original AMIE C_u covariance model
	it is also the main tuning paramenter for
	the AMIENext procedure, so different
	models can subclass this class
	"""
	def __init__(self,locations):
		pass

class EOFBasedCovariance(object):
	"""
	EOF based covariance as in Equation 15 of Matsuo et. al., 2015
	"""
	def __init__(self,alpha_file,beta_file):
		"""
		Hand load the alpha and beta files
		"""
		self.EOFbasis = amie_basis.EmpericalOrthogonalFunctionSet()
		self.EOFbasis.load_alpha_beta_from_files(alpha_file,beta_file)
		alpha_all,beta_all = self.EOFbasis.alphas,self.EOFbasis.betas
		cu_pcs = self.EOFbasis.basis_pc_eigvecs # PCs of Richmond and Kamide Cu matrix

		d = alpha_all**2
		
		#Calculate the 'C sub gamma' diagonal matrix
		#which here is approx as the mean of the square of all alphas for each
		#EOF along the diagonal
		Cg = np.zeros((self.EOFbasis.n_eofs,self.EOFbasis.n_eofs))
		for i_eof in range(self.EOFbasis.n_eofs):
			#Only use alpha values below the 95th percentile in mean
			not_outliers = d[:,i_eof]<=np.nanpercentile(d[:,i_eof],95)
			Cg[i_eof,i_eof] = np.nanmean(d[not_outliers,i_eof])

		#print str(Cg)
		# Convert beta from Cu principal components to AMIE basis functions
		# (First 3 EOFs times 50 highest eigenvalues (by default))
		B = np.dot(cu_pcs,beta_all)

		#Covariance
		self.prior_model_covariance = np.dot(B,np.dot(Cg,B.T))
		

class AmieCuKp3Covariance(object):
	"""
	This is the simplest possible prior model error covariance model
	it simply loads the Richmond and Kamide error covariance from a 
	table
	"""
	def __init__(self):
		#Load the 244x244 covariance matrix that was the original one used for AMIE
		self.prior_model_covariance = np.loadtxt(os.path.join(cov_table_dir,'cu_kp3'))
		self.prior_model_covariance = .5e11 * self.prior_model_covariance 
		#Magic number .5e11 from ground magnetometer matlab amie...has to do with
		#units of electric potential?
		#I'm guessing that the cu_kp3 has units of kV**2, since Var(X) = Expectation((X-mu)**2)
		#then the unit conversion would be (1000 V/kV)...where the other .5e5 comes from I have no idea

class CS10Covariance(object):
	"""
	This is the covariance matrix constructed from 30 EOFs of the residuals:
	Residuals = (superDARN observations - CS10 Model Output).
	This is Ellen Cousins' work to characterize the deviations of the model
	from observations and thus the model error
	"""
	def __init__(self):
		import just_sam
		# Get coefficients that define EOFs
		eof_coeffs = just_sam.basis.load_eofs()
		(nbasis,neof) = eof_coeffs.shape
		
		#Construct EOF covariance matrix
		# (diagonal matrix with variance defined by power law:
		#  C[n,n] = a*n^-b , with
		#  power-law parameters a = 80 kV^2, b = 1)
		apl = 80.*1e6 #kV -> V
		bpl = 1.
		n = np.arange(neof)+1
		eof_var = apl*n**(-bpl)
		eof_cov = np.diag(eof_var)
		eof_cov_inv = np.diag(1./eof_var)

		#Prior model error covariance in terms of AMIE basis functions
		self.prior_model_covariance = np.dot(np.dot(eof_coeffs,eof_cov),np.transpose(eof_coeffs))
			
import amie_core
import amie_obs
import pytest, os
import numpy as np
from numpy import testing as nptest

#Where are the test data files for the different types of observations
test_table_dir = os.path.join(amie_core.tables_dir,'test_amie_obs')

def test_superMAG_reads_ascii():
	"""
	Does the SuperMAG ASCII parser read the data and format the output arrays
	as expected?
	"""
	#Test data
	fn = os.path.join(test_table_dir,'20160914-20-23-supermag.txt')
	sm = amie_obs.SuperMAG()
	dts,t,mlat,mlt,Bn,Be,Bz,station_cols = sm.parse_ascii(fn)
	#First station from 5th record in data file
	#2010	05	29	00	04	00	221
	#IAGA    Bn		  Be	 Bz  mlt    mlat    sza      ???
	#YKC	168.1	-11.5	6.5	15.66	69.42	19.55	60.49
	ykc_col = station_cols['YKC']
	fifth_record_manual = np.array([168.1,-11.5,6.5,15.66,69.42])
	fifth_record_parsed = np.array([y[4,ykc_col] for y in [Bn,Be,Bz,mlt,mlat]])
	nptest.assert_allclose(fifth_record_manual,fifth_record_parsed,atol=1e-4)

if __name__ == "__main__":
	#Run the tests
	pytest.main()
import numpy as np

def GaspariCohnCorrelationPolar(theta1,phi1,theta2,phi2,c):
	"""
	theta1,phi1 - n locations in polar coordinates (e.g. observation locations)
	theta2,phi2 - m locations in polar coordinates (e.g. grid points)
	c - the scaling parameter c in below reference
	
	Find the [n x m] correlation matrix between two sets of locations in polar coordinates
	using the generalized spline method:

		The family C0(z,b,c) used to produce Figs. 7&8 of 
		Gaspari & Cohn (1999) "Construction of correlation functions in
		two and three dimensions"
		
	Rename to amie_taper.py
	"""
	RI = 6.4812e6

	ktotal1 = len(theta1)
	ktotal2 = len(theta2)

	# Typical usage: c = 3000;  r = 0:1:6000;      
	# Corr = genspline0(r,c,.5);     (yields Eqn. 4.10 of Gaspari & Cohn (1999)   
	# c = RI*pi/8;    % 22.5 - 45 degree
	# c = RI*pi/10;   % 18   - 36 degree
	# c = RI*pi/12; % 15   - 30 degree
	# c = RI*pi/18; % 10   - 20 degree

	COR = np.zeros((ktotal1,ktotal2)) #Correlation matrix

	for k1 in range(ktotal1):
	  #Great Circle Distance Vector for the k1-th value of theta1 to each theta2 values
	  X = np.cos(theta1[k1])*np.cos(theta2) + np.sin(theta1[k1])*np.sin(theta2)*np.cos(phi1[k1]-phi2) 
	  X[X > 1.00] = 1.00 #?
	  lam = RI*np.arccos(X).flatten()
	  #print lam[lam<c*2];
	  COR[k1,:] = genspline0(lam,c,.5)

	return COR

def genspline0 (r,c,b):
	"""

	  Direct translation of MATLAB genspline0, by Greg Gaspari, into Python.     

	Inputs

		r - n x 1 numpy array
			The Euclidian distance from the point of interest

		c - float
			Characteristic distance for the correlation tapering off. Goes to about 20 percent of it's original
			value at r=1*c if b = 1/2, is zero by r=2*c

		b - float
			Slope parameter (see original comments). If b = 1/2 then the function is the 2D equivalent of a B-spline

	Returns

		Corr - n x1 numpy array

	Original Comments:

	  Typical usage: c = 3000;  r = 0:1:6000;      
					 Corr = genspline0(r,c,.5);     (yields Eqn. 4.10 of Gaspari & Cohn (1999) 
					 Corr = genspline0(r,1000,-.1); (yields the dot-dash curve in Fig. 7, GC 1999)
	 
	  Code to find the family C0(z,b,c) used to produce Figs. 7&8 of 
	  Gaspari & Cohn (1999) "Construction of correlation functions in
							 two and three dimensions"
	
	  GENSPLINE0 (R,L,B) returns the generalized spline correlation function.
	  This two-parameter function is given by:
	
	 C(z) := fi(z/c),   (i-1)c/2 < z <= ic/2,  c = L*sqrt(10/3),  i = 1,2,3,4   (*) where
	
	   f1(z) = -[48 bz^4 -48 b^2z^4 + 60 b^z^3 - 9 b + 160 b^2z^2 - 40 bz^2
	 -40 bz^2 - 66 b^2 -120 b^2z^3 + 20 z^2 + 56 b^2z^5 - 64 bz^5 - 15 z^3 + 24 z^5%   -24 z^4 -3] / [3*norm],   0 <= z <= 1/2,
	
	   f2(z) = [-120 z^2 - 96 z^5 + 32 z^6 - 4 + 48 z + 29 b - 42 b^2 + 80 z^3
	   + 160 b^2z^6 - 192 bz^6 + 60 z^4 + 612 b^2z - 880 bz^3 + 800 b^2z^3
	   - 1080 b^2z^2 + 780 bz^2 - 210 bz - 384 b^2z^5 + 480 bz^5] / [12 z*norm], 1/2 < z <= 1,
	
	  f3(z) = -b [243 - 230 b + 96 bz^6 - 64 z^6 - 720 z^3 + 1620 z^2 - 1134 z
	   + 732 bz - 1320 bz^2 + 240 bz^4
	   + 288 z^5 - 384 bz^5 - 240 z^4 + 800 bz^3]/[12 z*norm],  1 < z <= 3/2,
	
	   f4(z) = 4 b^2 [ -120 z^2 - 16 + 96 z + 40 z^3
		+ 2 z^6 - 12 z^5 + 15 z^4] / [3 z*norm],  3/2 <= z <= 2,    (**)
	
	 where   norm = 3 b + 22 b^2 + 1.
	
		   C is the cutoff distance in Eq. (*) .
		   B is the slope parameter in Eq. (**) (dimensionless).
	
	  Greg Gaspari,  March 25, 1996
	  Last Modified:  Nov. 30, 2000     

	"""
	b,c = float(b),float(c) #Make sure we're not doing integer arithmetic

	norm =  3*b + 22*b*b + 1
	z = r/c                        # scale to the support [0,2]
	Corr = np.zeros_like(z)         # initialize

	#f1 is applicable over z in [0,.5]
	i = np.flatnonzero(np.logical_and(z >= 0,z <= .5))
	Corr[i] = 48*b*z[i]**4 - 48*b*b*z[i]**4 + 60*b*z[i]**3 - 9*b \
		+ 160*b*b*z[i]*z[i] - 40*b*z[i]*z[i] -66*b*b \
		- 120*b*b*z[i]**3 + 20*z[i]*z[i] + 56*b*b*z[i]**5 \
		- 64*b*z[i]**5 - 15*z[i]**3 + 24*z[i]**5 - 24*z[i]**4 - 3
	Corr[i] = - Corr[i]/(3*norm)

	#f2 is applicable over z in [.5,1.0]
	i = np.flatnonzero(np.logical_and(z >= .5,z <= 1.0))
	Corr[i] = -120*z[i]*z[i] - 96*z[i]**5 + 32*z[i]**6 - 4 \
		+ 48*z[i] + 29*b - 42*b*b + 80*z[i]**3 + 160*b*b*z[i]**6 \
		- 192*b*z[i]**6 + 60*z[i]**4 + 612*b*b*z[i] - 880*b*z[i]**3 \
		+ 800*b*b*z[i]**3 - 1080*b*b*z[i]*z[i] + 780*b*z[i]*z[i] \
		- 210*b*z[i] - 384*b*b*z[i]**5 + 480*b*z[i]**5
	Corr[i] = Corr[i]/(12*z[i]*norm)

	#f3 is applicable over z in [1.,1.5]
	i = np.flatnonzero(np.logical_and(z >= 1.0,z <= 1.5))
	Corr[i] = - b* ( 243 - 230*b + 96*b*z[i]**6 - 64*z[i]**6 \
		- 720*z[i]**3 + 1620*z[i]*z[i] - 1134*z[i] + 732*b*z[i] \
		- 1320*b*z[i]*z[i] + 240*b*z[i]**4 + 288*z[i]**5 \
		- 384*b*z[i]**5 - 240*z[i]**4 + 800*b*z[i]**3 )
	Corr[i] =  Corr[i]/(12*z[i]*norm)

	#f4 is applicable over z in [1.5,2.0]
	i = np.flatnonzero(np.logical_and(z >= 1.5,z <= 2.0))
	Corr[i] = 4*b*b* (-120*z[i]*z[i] - 16 + 96*z[i] \
		 + 40*z[i]**3 + 2*z[i]**6 - 12*z[i]**5 + 15*z[i]**4 )
	Corr[i] = Corr[i]/(3*z[i]*norm)

	return Corr
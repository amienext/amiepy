import amie_taper
import pytest
import numpy as np
from numpy import testing as nptest

def test_genspline0_returns_same_as_matlab():
	"""
	Does the Gaspari & Cohn correlation function genspline0
	return the same output as the equivalent MATLAB command?
	"""
	#Tested using MATLAB
	expected_corr = [0,0.0105,0.1384,0.5103,0.9256,0.9256,0.5103,0.1384,0.0105,0]
	r = np.abs(np.linspace(-2000.,2000.,10))
	b = .5
	c = 1000. #km
	corr = amie_taper.genspline0(r,c,b)
	nptest.assert_allclose(corr,expected_corr,atol=1e-04)

if __name__ == "__main__":
	#Run the tests
	pytest.main()
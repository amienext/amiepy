import os
import amie_core
import numpy as np
import ovationpyme
import matplotlib.pyplot as pp
from geospacepy import satplottools
import scipy.linalg as linalg

class BasisFunctionSet(object):
	"""
	A class representing n_basis AMIE basis functions

	These basis functions are described in:
	Richmond and Kamide, 1988


	
	Legacy information about AMIE Basis Functions (From Art Richmond's qgen.f):
	C
	C  This program generates the basis functions for AMIE.
	C  Most of the program is in terms of co-latitude THETA (TH)
	C  ITHMX=45 is the equator, and implies that DLAT is 2 deg (90/45 = 2)
	C  ITHTRNS*DLAT is the transition co-latitude, below which the basis
	C    functions become smooth.  Was originally set to 17 (17*2=34, or 56 mlat)
	C  Changed in 4/91 to 23 to accomodate very active days.  23*2=46 or 44 mlat,
	C   or 23*1.67 = 38.3 or 51.7 mlat depending on what ITHMX is.
	C  ITHMX=54 means DLAT = 90/54 = 1.67 deg
	C  4/93:  To have basis funcs go down to 40 mlat, then ITHTRNS*DLAT = 50,
	C         ITHTRNS = 50 / 1.67 = 30.
	C  360 / MMX = minimum wavelength used.  360/10=36, 360/11=32.7, 360/12=30.
	C  In AMIE, choose DLAT same as here, and choose DLON ~ min wavelenth / 3
	C  Generally, want ITHPLT of AMIE to go down to 50 mlat, or ITHPLT*DLAT=40.
	C  For 196 coefs, DLAT=2, min_wavel=33, ITHPLT=20, and LONMX=24.
	C  For 244 coefs, DLAT=1.67, min_wavel=30, ITHPLT=24, and LONMX=36.
	C  4/93:  To have plots and basis funcs go down to 40 mlat, ITHPLT*DLAT=50,
	C   or for 244 coefs, DLAT=1.67, min_wavel=30, ITHPLT=30, and LONMX=36.
	C
	C  MMX = # of wave numbers in longitude
	C   MXNMX .GE. MMX+1
	C   NMX(M=0-MMX) = MXNMX - M   (i.e., NMX(MMX-0) = MXNMX to MXNMX-MMX)
	C   NQS = SUM(NMX)
	C   NQ = 2*NQS
	C   MCOEF = NQ - MXNMX = # of coefficients
	C   NCOEF = 2*MCOEF + 2
	C  MROW =
	C   KMX=MROW*2.
	C  ITHTRNS = colat of transition between high and low latitudes
	C

	We are using qset244.24, which has the following settings:
	C  The following dimensions are for 244 basis funcs, down to 52 deg:
	C  Parameters for 244 coefs, basis funcs to 51.7 mlat.  DLAT=90/54=1.67,
	C     min_wavel=360/12 = 30 deg  so  DLON ~ 30/3 = 10 deg or LONMX=36.
	C     PARAMETER (ITHMX=54,LONMX=36)
	C     PARAMETER (MMX=12,NQ=260,NQS=130,MXNMX=16,MCOEF=244,NCOEF=490)      PARAMS.40
	C     PARAMETER (MROW=60,KMX=120,ITHTRNS=24)                              PARAMS.42

	"""
	def __init__(self,conductance_model=None):
		"""
			conductance_model - object, optional 
				The conductance model to use for calculation of 
				(equivalent) current from electric field, must support
				the following syntax:

				cond_ped,cond_hall = conductance_model.get_conductance(mlats,mlons)

				for 2D latitude and longitude arrays, returning same-shape 2D conductance arrays
		"""
		self.conductance_model = conductance_model
		self.bfcn_tables_path = os.path.join(amie_core.tables_dir,'amieqset')
		self.qmeta = self.read_qset_metafile()
		self.ibm,self.iem = self.read_qset_m_layout_file()
		self.ns,self.nmx,self.nss = self.read_qset_n_layout_file()
		self.q,self.dq = self.read_qset()
		self.vfmps,self.zfmps = self.read_groundmag_zfmps_vfmps()
		self.nnp1 = self.read_laplacian_factor_file()

		# Colatitues of original grid. These are a property of the 
		# basis set
		self.theta = np.arange(1.,self.qmeta['ithmx'])*self.qmeta['dth']
		self.lats = 90.-np.degrees(self.theta)

		# The 'natural' longitudes (not a property of the basis set, but derived from it):
		# Longitudes spaced at close to the smallest resolution resolvable by
		# the f_m (azimuthal) portion of the basis set (i.e. there are as many
		# longitude points as sine or cosine cycles in the highest azimuthal resolution
		# basis function)
		self.lons = np.linspace(0.,360.,36) 
		self.phi = np.radians(self.lons)

		#Create the 'natural' auroral grid for this basis function set
		#This is not the only grid that can be used, but it is
		#in a sense the minimum total number of grid points that
		#can fully exploit all of the basis functions' variablility
		#	This grid only goes down to the 'transfer' latitude at which the amie basis functions
		#	cease being spherical harmonic and are simply decaying trigenometric functions, hence 'auroral'
		self.grid_lats = self.lats[:self.qmeta['ithtrns']]
		self.lat_grid,self.lon_grid = np.meshgrid(self.grid_lats,self.lons,
						indexing='ij') # i,j indexing means grids of shape (M,N) for len(lats)=M, len(lons)=N)

		#Standarize our apex reference radius, in meters. 
		self.R_ionosphere = 6.3712e6+110000. 

	def read_qset_metafile(self,fname='qset16ascii24424bf_4.dat'):
		"""
		Read the file which contains the 'metadata' about the basis function
		set, such as maximum values and lengths. These values are single-value constants.
		"""
		fname = os.path.join(self.bfcn_tables_path,fname)
		if not os.path.exists(fname):
			raise IOError("Unable to access 4th AMIE basis function table file %s" % (fname))
		
		with open(fname,'r') as f4:
			data4 = f4.readline().split()

		#i theta max, length of colatitudes array (for qset244.24 ithmx=54)
		ithmx = int(data4[0])
		# ? (for qset244.24 mrow=60)
		mrow = int(data4[1])
		#K is normalization constant for basis function ? (qset244.24 kmx=120)
		kmx = int(data4[2])
		#Maximum 'm' value for basis function set 
		#(m takes values in -mmx,-mmx+1,...,0,...,mmx-1,mmx) (12 for qset244.24)
		mmx = int(data4[3])
		#Total number of distinct m,n combinations (2*nqs) 
		#(nqs = sum(mxnmx-|m|) for m in -mmx:mmx = 260 for qset244.24)
		nq = int(data4[4])
		#Total number of distinct |m|,n combinations (nqs/2) (130 for qset244.24)
		nqs = int(data4[5])
		#Maximum number of n values per m value, i.e. there are mxnmx n values for m=0 (16 for qset244.24)
		mxnmx = int(data4[6])
		#Index into array of colats at which basis functions go from Legendre (high lat) to Trigonetric (low lat) (24 for qset244.24)
		ithtrns = int(data4[7])
		#Step size in colatitude in radians (0.29089E-01 for qset244.24)
		dth = float(data4[8])
		qmeta = {'ithmx':ithmx,'mrow':mrow,'kmx':kmx,'mmx':mmx,'nq':nq,
					'nqs':nqs,'mxnmx':mxnmx,'ithtrns':ithtrns,'dth':dth}
		return qmeta

	def read_qset_m_layout_file(self,fname='qset16ascii24424bf_2.dat'):
		"""
		Read the table file which describes which basis function number (0-243) 
		corresponds to each value of 'm',
		from -mmx to mmx, including zero (2*mmx+1) values, 
		in equation 38 of Richmond and Kamide, 1988.
		"""
		fname = os.path.join(self.bfcn_tables_path,fname)
		if not os.path.exists(fname):
			raise IOError("Unable to access 2nd AMIE basis function table file %s" % (fname))
		
		with open(fname,'r') as f2:
			data2 = np.loadtxt(f2,dtype='int')

		#ibm[i] is starting index into q and dq of all of the n values 
		#for m = i, iem[i] is the ending index

		ibm = data2[:,0]-1 #from 1-based to 0-based indexing
		iem = data2[:,1]-1

		return ibm,iem

	def read_qset_n_layout_file(self,fname='qset16ascii24424bf_3.dat'):
		"""
		metadata file has following columns
		0. ns - row number in q of first n value for this m
		1. nmx - maximum number of n values for this m value (nmx = mxnms-|m|)
		2.   
		for each value of m in [-mmx,-mmx+1,...,0,...,mmx-1,mmx]
		
		ns is important because it describes the real organization of dq and q arrays
		which is slightly tricky because all of the data for the basis functions
		for say m = 1 is intermingled with m = -1 in an alternating fasion. The
		following table attempts to describe how each successive row in q and dq
		associate with and m and n values for the basis functions

		row #, m, n
		-----------
		ns(1)+1,1,n(1)[0]  # NOTE: Check if -m is first or +m is first, not clear
		ns(1)+2,-1,n(1)[0]
		ns(1)+3,1,n(1)[1]
		ns(1)+4,-1,n(1)[1]
		...
		ns(1)+2*nmx(1),1,n(1)[nmx(1)]
		ns(1)+2*nmx(1)+1,-1,n(1)[nmx(1)]

		where,
			ns - starting row for the block of rows which belong collectively to +m/-m
			n[i] - ith n value for +m/-m  
			nmx - mxnmx-|m|, the number of n values for +m/-m
		"""
		fname = os.path.join(self.bfcn_tables_path,fname)
		if not os.path.exists(fname):
			raise IOError("Unable to access 3rd AMIE basis function table file %s" % (fname))

		with open(fname,'r') as f3:
			data3 = np.loadtxt(f3,dtype='int')
		
		ns  = data3[:,0]-1 #from 1-based to 0-based indexing
		nmx = data3[:,1]
		nss = data3[:,2]-1 #minus 1 for zero based indexing?

		return ns,nmx,nss

	def read_qset(self,fname='qset16ascii24424bf_1.dat'):
		"""
		Load the precomputed basis functions values from an ASCII table.
		These basis sets were computed using very high floating point
		precision on a Cray supercomputer, which is why we use
		tabulated values instead of compute ourselves at this point

		For the qset244.24, this a 520 row, 55 column data matrix

		Each column represents values for one latitude in 
			[90,90-1.67,90-2*1.67,...,54]

		The rows are divided into two sets of 260, the values for the
		basis functions first, and values for their derivatives WRT colatitude
		after.
		"""
		# Set parameters
		# qset244.24
		# the following parameters are for 36 basis functions, down to 44deg
		# From A. Richmond, NCAR-HAO

		fname = os.path.join(self.bfcn_tables_path,fname)
		if not os.path.exists(fname):
			raise IOError("Unable to access primary AMIE basis function table file %s" % (fname))
		
		with open(fname,'r') as f:
			data = np.loadtxt(f,dtype=np.float64)
		
		qmeta = self.read_qset_metafile()
		nq,ithmx = qmeta['nq'],qmeta['ithmx']

		q  = data[0:nq,1:ithmx+1]     # using 1:ithmx+1 rather than 0:ithmx+1
		dq = data[nq:2*nq,1:ithmx+1]  # to match bug in original code
		return q,dq

	def read_groundmag_zfmps_vfmps(self,fname_v='qsetz244_vfmps.dat',fname_z='qsetz244_zfmps.dat'):
		"""
		Load the set of precomputed constants which are used in transforming the
		AMIE basis function weights/coefficients which represent the expansion
		of electric potential (i.e. c_n^m) to those that can represent magnetic
		perturbations at the ground (in the D,H,Z directions).

		I have not yet been able to find a reference describing how these are computed
		but they depend only on the 'n' index in the AMIE basis functions.
		There are 130 rows for each of these (16+15+14+...+4=130) (the number 
		of n eigenvalues for each |m|, beginning with 16 values of n for m=0), 
		in the qset244 version of these tables.
		
		vfmps table is for D and H (horizontal components of ground mag coord system)
		zfmps table is for Z (vertical component of ground mag basis coord system)	

		Note that these were computed by a different FORTRAN code than
		the rest of the tables ( beginning with qsetascii24424 ). 

		"""

		fname_v = os.path.join(self.bfcn_tables_path,fname_v)
		fname_z = os.path.join(self.bfcn_tables_path,fname_z)
		
		if not os.path.exists(fname_v) or not os.path.exists(fname_z):
			raise IOError("Unable to access AMIE basis function ground magnetic perturbation file %s and %s" % (fname_v,fname_z))

		with open(fname_v,'r') as f:
			data_v = np.loadtxt(f,dtype=np.float64)
		with open(fname_z,'r') as f:
			data_z = np.loadtxt(f,dtype=np.float64)
		
		return data_v,data_z

	def read_laplacian_factor_file(self,fname='nnp1.244.24'):
		"""
		This table appears to be a factor for each basis function
		which allows the expression of the horizontal laplacian (del squared)
		of the basis function set. It has shape 1 x n_basis (244)
		"""

		fname = os.path.join(self.bfcn_tables_path,fname)
		if not os.path.exists(fname):
			raise IOError("Unable to access AMIE basis function laplacian file %s." % (fname))

		with open(fname,'r') as f:
			data = np.loadtxt(f,dtype=np.float64)

		return data

	def compute_f(self,mlon,mmx=None,derivative=False):
		"""
		Computes the values of the azimuthal part of the AMIE basis functions
		for an array of longitudes (phi):
			f_m(phi) 
				= sqrt(2)cos(m*phi) m < 0 
				= sqrt(2)sin(m*phi) m > 0 
			
			for each m in [-mmx,-mmx+1,...,0,...,mmx-1,mmx]
		
		Converted from AMIE FORTRAN originally by A. Richmond, NCAR-HAO
		where this was a subroutine called fcmp

		Inputs 
			mlon - np.ndarray 
				Longitude, in degrees
			mmx - int, optional
				Maximum value of m for AMIE basis functions
				If not defined, use default mmx from qset
			derivatives - bool, optional
				Also return the derivative of f WRT longitude/phi

		Returns 

			f - np.ndarray,shape=(2*mmx+1, len(mlon))
				Values of azimuthal component of basis functions
				at all longitudes, for all m
		"""
		# Internal function
		# 
		# They are apparently built up with some kind
		# of recursive algorithm
		phi = np.radians(mlon)
		cp,sp = np.cos(phi),np.sin(phi)
		nlons = len(phi)
		
		if mmx is None:
			mmx = self.qmeta['mmx']

		fp = np.ndarray(shape=(mmx+1,nlons),dtype=np.float64)
		fm = np.ndarray(shape=(mmx+1,nlons),dtype=np.float64)

		fp[0,:] = 1.
		fm[0,:] = 1.
		fp[1,:] = np.sqrt(2)*sp 
		fm[1,:] = np.sqrt(2)*cp

		for m in range(2,mmx+1):
			fp[m,:] = cp*fp[m-1,:] + sp*fm[m-1,:] # f for m > 0 at each of the nlont longitudes
			fm[m,:] = cp*fm[m-1,:] - sp*fp[m-1,:] # f for m < 0 at each of the nlont longitudes

		f = np.vstack((fm[mmx:0:-1,:],fp[0:mmx+1,:]))
		
		if not derivative:
			
			return f

		else:
			
			df = f.copy()
			ms = np.arange(-mmx,mmx+1)
			for m in ms:
				ind_this_m = m+mmx
				ind_this_minus_m = 2*mmx-ind_this_m
				df[ind_this_m,:] = m*f[ind_this_minus_m,:]	
			
			return f,df

	def get_basis_spline_weights(self,lats):
		"""
		Use spline interpolation on the AMIE latitude grid
		for each given lat, so that we can get a linear combination
		of the basis function for each m at adjacent latitudes
		that best represents the weights for each m
		value basis subset for each given lat
		"""
		theta = np.radians(90.-lats)
		
		#x represents a 'latitude index'
		x = theta/self.qmeta['dth'] 
		#ith is the closest index into the AMIE basis latitude grid to each latitude 
		#in lats
		ith = x.copy()
		#I changed the upper limit to -3 to debug an index error
		#ith = np.around(np.clip(ith,1,self.qmeta['ithmx']-2)).astype(int)
		ith = np.around(np.clip(ith,1,self.qmeta['ithmx']-3)).astype(int)

		#x is now distance in index units between each value in lats and it's closest
		#grid latitude (ith)
		x = x - ith

		#Now perform a type of spline interpolation in latitude direction
		xm1 = x*(-2. + x*(3. - x))/6. #x minus 1
		x0  = 1.+ x*(-.5 + x*(-1. + .5*x)) #x
		xp1 = x*( 1. + x*(.5 - .5*x)) #x plus 1
		xp2 = x*(-1. + x*x)/6# x plus 2
		return ith,xm1,x0,xp1,xp2

	def get_potential(self,lats,lons,derivatives=False):
		"""
		Get the amie basis function values at locations lats,lons for 
		the potential (and optionally, it's derivatives)

		Inputs:
			lats - np.ndarray, nloc
				Array of Apex latitudes 
			lons - np.ndarray, nloc
				Array of Apex longitude
			derivatives - bool, optional
				Return also the partials of the potential WRT
				latitude (theta) and longitude (phi)
		
		Returns:

			if not derivatives:
				
				pots - np.ndarray, shape=(nloc,nbasis=244)
					Value of each of the 244 basis functions
					at each of the nloc locations 
				
			if derivatives:
			
				pots, dpots_dth, dpots_dph - np.ndarray, shape=(nloc,nbasis=244)
					Value of potential and it's derivatives
					for each of the 244 basis functions
					at each of the nloc locations 


		"""
		if lats.shape != lons.shape:
			raise ValueError('Latitude array shape (%s) and longitude array shape (%s) must be equal' % (str(lats.shape),str(lons.shape)))

		mmx = self.qmeta['mmx']
		nlocs = len(lons)
		
		#Create arrays for the potential and it's derivatives (if we want them)
		pots = np.ndarray(shape=(nlocs,self.iem[2*mmx]+1),dtype=np.float64)
		if derivatives:
			dpots_dth = np.ndarray(shape=(nlocs,self.iem[2*mmx]+1),dtype=np.float64)
			dpots_dph = np.ndarray(shape=(nlocs,self.iem[2*mmx]+1),dtype=np.float64)
		
		#Always get the derivative, because it doesn't cost much extra and
		#makes the rest of the code more readable
		f_m,df_m = self.compute_f(lons,derivative=True)

		ith,xm1,x0,xp1,xp2 = self.get_basis_spline_weights(lats)
		
		all_m = range(-mmx,mmx+1)
		for i_m,m in enumerate(all_m):
			# f_m for all longitudes for this m
			this_f_m = f_m[i_m,:]
			# derivative of f_m wrt longitude for this m (only used if derivatives is True)
			this_df_m = df_m[i_m,:]

			#Find the basis function numbers which correspond to all basis functions 
			#for this value of m
			i_basisfcn_m = range(self.ibm[i_m],self.iem[i_m]+1)

			row_n_start = self.ns[abs(m)] # Row at which the n values start for this m
			# nmx(m) = mxnmx-|m| 
			for i_n,i_basisfcn in enumerate(i_basisfcn_m):
				# Q We are skipping one row for each value of m...why? Because only use half of the basis functions in
				# this sum
				this_n_row = row_n_start+2*i_n # This would be symmetric, i.e. m < 0, so f_m~cos(m*phi)
				
				#Part of potential that varies with theta (latitude)
				this_pot_th = (xm1*self.q[this_n_row,ith-1] +
							   x0*self.q[this_n_row,ith] +
							   xp1*self.q[this_n_row,ith+1] +
							   xp2*self.q[this_n_row,ith+2])
				
				#Part of potential that varies with phi (longitude) is f_m
				pots[:,i_basisfcn] = this_pot_th*this_f_m
				
				if derivatives:
					#Derivative of part of potential that varies with theta (latitude) WRT theta
					this_dpot_dth = (xm1*self.dq[this_n_row,ith-1] +
								   x0*self.dq[this_n_row,ith] +
								   xp1*self.dq[this_n_row,ith+1] +
								   xp2*self.dq[this_n_row,ith+2])
					
					#Partial derivative WRT phi is just theta part times phi derivative of f_m
					dpots_dph[:,i_basisfcn] = this_pot_th*this_df_m
					#Partial derivative WRT theta is theta derivative of theta part times f_m
					dpots_dth[:,i_basisfcn] = this_dpot_dth*this_f_m

		#Return based on derivatives settings
		if not derivatives:
			return pots
		else:
			return pots,dpots_dph,dpots_dth

	def get_magnetic_perturbations(self,obs,obs_subset=None):
		"""
		Get the AMIE basis functions (and their derivatives)
		at the locations in Observations class instance obs (obs.lats,obs.lons)
		and then apply the appropriate transformations so that they can represent magnetic perturbations
		instead of potential ( i.e. B = grad(V) )

		Have to use an Observations class instance, because we need the Apex basis vectors (d,e and f) and space-varying
		quantities like the pseudo-inclination sin Im to compute the perturbations 

		obs_subset - np.ndarray, optional
			A boolean array which the caller can provide to only select a portion of the subset. Must have shape (obs.lats.shape[0],),
			i.e. have the same number of elements as the number of rows in the observations

		"""
		#Handle the possibility that we only want a subset of the observations in obs
		#if this is not set, just use an array that selects all values (with finite apex latitude)
		#which should be everything
		g = obs_subset if obs_subset is not None else np.isfinite(obs.lats)
		g = g.flatten() #Make sure selection array can be use to select rows

		#Get the potential derivatives
		pot,dpotdph,dpotdth = self.get_potential(obs.lats[g].flatten(),obs.lons[g].flatten(),derivatives=True)
		f1,f2 = obs.f1[g,:],obs.f2[g,:]
		e1,e2 = obs.e1[g,:],obs.e2[g,:]
		sinIm,Rearth,Rref = obs.sinIm[g],obs.Re*1000.,obs.Re*1000.+obs.hr*1000.

		sinqdlat = np.sin(np.radians(obs.qdlats[g].flatten()))
		cosqdlat = np.cos(np.radians(obs.qdlats[g].flatten()))

		aqfac    = np.abs(sinqdlat)/np.sqrt(sinqdlat*sinqdlat + Rref/Rearth)

		hemisign = np.sign(np.sum(obs.lats[g].flatten())) #The calculation is different by a minus sign in the southern hemisphere

		#Rref_m = Rref*1000. # Apex Reference Radius in Meters

		term_d1_ph = (e1[:,0]*f2[:,0])+(e1[:,1]*f2[:,1])/cosqdlat/Rref # fac_dlon_e1f2
		term_d1_th = (e1[:,0]*f1[:,0])+(e1[:,1]*f1[:,1])*aqfac/Rref     # fac_dlat_e1f1
		term_d2_ph = (e2[:,0]*f2[:,0])+(e2[:,1]*f2[:,1])/cosqdlat/Rref  # fac_dlon_e2f2
		term_d2_th = (e2[:,0]*f1[:,0])+(e2[:,1]*f1[:,1])*aqfac/Rref     # fac_dlat_e2f1
		
		dBd1,dBd2 = np.zeros_like(pot),np.zeros_like(pot)
		dBd1.fill(np.nan)
		dBd2.fill(np.nan)
		for i_basisfcn in range(dpotdph.shape[1]):
			dBd1[:,i_basisfcn] = term_d1_ph*dpotdph[:,i_basisfcn]/Rref + hemisign*term_d1_th*dpotdth[:,i_basisfcn]/Rref
			dBd2[:,i_basisfcn] = term_d2_ph*dpotdph[:,i_basisfcn]/Rref + hemisign*term_d2_th*dpotdth[:,i_basisfcn]/Rref

		#To nT
		dBd1,dBd2 = dBd1*1.0e7,dBd2*1.0e7
		
		return dBd1,dBd2

	def calc_potcoeff_to_groundmagcoeff(self,cond_hall=None,cond_ped=None,nlons=None,fma_dtype=np.float64):
		"""
		Compute the n_basis x n_basis matrices (one for vertical (Z), one for 
		horizontal (D,H) components) which map AMIE basis function coefficents 
		which can represent the electric potential (at Apex reference height)
		to those which can be used to represent ground magnetic perturbations
		
		cond_hall, np.ndarray shape = (self.qmeta['ithmx'],nlons), optional
			Hall conductance for latitudes of the AMIE grid, with a variable number of equally spaced longitudes
			If not specified will calculate using self.conductance_model.get_conductance()

		cond_ped, np.ndarray shape = (self.qmeta['ithmx'],nlons), optional
			Hall conductance for latitudes of the AMIE grid, with a variable number of equally spaced longitudes
			If not specified will calculate using self.conductance_model.get_conductance()
		
		nlons, int,optional
			number of longitudes to compute for the grid over which
			numerical integrations will be performed. By default (if it stays as None)
			it is set to 36 or the maximum azimuthal wavenumber of any of the basis functions
		"""
		
		mmx = self.qmeta['mmx']
		nbasis = self.iem[2*mmx]+1

		#Colatitudes in radians of qset lat grid
		thetas,lats = self.theta,self.lats
		#Difference between adjacent thetas in original grid
		dth = self.qmeta['dth']
		#Longitudes (used for numerical 'integration')
		if nlons is None:
			nlons = 36
		
		dlon = 360./nlons
		#lons = np.linspace(dlon,360.,nlons) # This will produce identical output at every step to Fortran code
		lons = np.linspace(0.,360.,nlons+1)
		
		if cond_hall is None and cond_ped is None:
			lat_grid,lon_grid = np.meshgrid(lats,lons,indexing='ij') # i,j indexing means grids of shape (M,N) for len(lats)=M, len(lons)=N
			cond_ped,cond_hall = self.conductance_model.get_conductance(lat_grid,lon_grid)
			#Store conductance for debugging purposes
			self.cond_lat,self.cond_lon,self.cond_ped,self.cond_hall = lat_grid,lon_grid,cond_ped,cond_hall
		elif not (cond_ped is not None and cond_hall is not None):
			raise RuntimeError('Bad conductance inputs, both cond_ped and cond_hall must be specified and have shape of AMIE full grid')

		n_amie_lats = len(self.q[0,:]) #Should also equal self.qmeta['ithmx']

		q_sym = np.zeros((nbasis,n_amie_lats),dtype=fma_dtype)
		dq_sym = np.zeros((nbasis,n_amie_lats),dtype=fma_dtype)
		q_asym = np.zeros((nbasis,n_amie_lats),dtype=fma_dtype)
		dq_asym = np.zeros((nbasis,n_amie_lats),dtype=fma_dtype)
		intCur_th = np.zeros((nbasis,2*mmx+1,n_amie_lats),dtype=fma_dtype)
		intCur_ph = np.zeros((nbasis,2*mmx+1,n_amie_lats),dtype=fma_dtype)
		
		#Get azimuthal part of basis functions and their derivitives wrt longitude/phi
		#evaluated at longitudes lons 
		f_m,df_m = self.compute_f(lons,derivative=True)

		all_m = range(-mmx,mmx+1)
		for i_m,m in enumerate(all_m):

			# f_m for all longitudes for this m
			this_f_m = f_m[i_m,:]
			# derivative of f_m wrt longitude for this m 
			this_df_m = df_m[i_m,:]

			#Find the basis function numbers which correspond to all basis functions 
			#for this value of m
			i_basisfcn_m = range(self.ibm[i_m],self.iem[i_m]+1)
			
			for i_n,i_basisfcn in enumerate(i_basisfcn_m):
			
				row_n_start = self.ns[abs(m)] # Row at which the n values start for this m

				this_n_row_sym = row_n_start+2*i_n # This would be symmetric, i.e. m < 0, so f_m~cos(m*phi)
				this_n_row_asym = row_n_start+2*i_n+1 # This would be antisymmetric, i.e. m > 0, so f_m~sin(m*phi)

				# nmx(m) = mxnmx-|m| 
				q_sym[i_basisfcn,:],dq_sym[i_basisfcn,:] = self.q[this_n_row_sym,:],self.dq[this_n_row_sym,:]
				q_asym[i_basisfcn,:],dq_asym[i_basisfcn,:] = self.q[this_n_row_asym,:],self.dq[this_n_row_asym,:]

				for ith,theta in enumerate(thetas):
					for ilon,lon in enumerate(lons):
						#The +1 is due to a off-by-one in the Fortran where the first row and column of the
						#conductance is ignored
						SigP,SigH = cond_ped[ith,ilon],cond_hall[ith,ilon]
						E_th = -1.*dq_sym[i_basisfcn,ith]*this_f_m[ilon]/self.R_ionosphere
						E_ph = -1.*q_sym[i_basisfcn,ith]*this_df_m[ilon]/(self.R_ionosphere*np.sin(theta))
						Cur_th = SigP*E_th + SigH*E_ph
						Cur_ph = SigP*E_ph - SigH*E_th
						#if debug:
						#	if ith <= 4 and ilon == nlons-1 and m==12 and i_n==0:
						#		print "--------------------"
						#		print "i_basisfcn= %d, m = %d, i_n = %d" % (i_basisfcn,m,i_n)
						#		print "f_m: %e, df_m: %e" % (this_f_m[ilon],this_df_m[ilon])
						#		print "Qsym: %e, dQsym: %e" % (q_sym[i_basisfcn,ith],dq_sym[i_basisfcn,ith])
						#		print "Lat:%f,Lon:%f" % (90.-np.degrees(theta),lon)
						#		print "E Efield: %e, N Efield %e" % (E_ph,E_th)
						#		print "E Current: %e, N Current %e" % (Cur_ph,Cur_th)
						#		print "SigP:%f, SigH:%f" % (SigP,SigH)
						#		print "Re*sin(theta)= %f" % (self.R_ionosphere*np.sin(theta))
						#Don't know what this is doing still, just copied over from FTran
						intCur_th[i_basisfcn,:,ith] = intCur_th[i_basisfcn,:,ith] + Cur_th*f_m[:,ilon]/nlons
						intCur_ph[i_basisfcn,:,ith] = intCur_ph[i_basisfcn,:,ith] + Cur_ph*f_m[:,ilon]/nlons

		#test_i_basisfcn = 8
		#test_ith = 8
		#for i_m,m in enumerate(all_m):
		#	print "I=%d, lat=%.2f, M=%d, Longitude Integrated Current: E: %e N: %e" % (test_i_basisfcn,
		#		lats[test_ith],m,
		#		intCur_ph[test_i_basisfcn,i_m,test_ith],intCur_th[test_i_basisfcn,i_m,test_ith])
	
		#I haven't yet found exactly what these are
		#they are related to the equivalent current functions
		TFMA = np.zeros((nbasis,nbasis),dtype=fma_dtype)
		PSFMA = np.zeros((nbasis,nbasis),dtype=fma_dtype)
		ZFMA = np.zeros((nbasis,nbasis),dtype=fma_dtype)
		VFMA = np.zeros((nbasis,nbasis),dtype=fma_dtype)

		for ith,theta in enumerate(thetas):
			
			#Sum over m,n
			for i_m,m in enumerate(all_m):
				i_neg_m = len(all_m)-i_m-1 # index of -m
				i_basisfcn_m = range(self.ibm[i_m],self.iem[i_m]+1)
				for i_n,i_basisfcn in enumerate(i_basisfcn_m):
					#Integrate over theta
					#TFMA - transformation matrix taking coefficeints for electric potential? 
					#to coefficients for Tau, the 'current potential' 
					#I_p = -grad(tau) where I_p is the 'potential current', AKA current which closes FACs
					TFMA[i_basisfcn,:] = TFMA[i_basisfcn,:] - dq_sym[i_basisfcn,ith]*intCur_th[:,i_m,ith]*self.R_ionosphere*dth*np.sin(theta)
					TFMA[i_basisfcn,:] = TFMA[i_basisfcn,:] - q_sym[i_basisfcn,ith]*intCur_ph[:,i_neg_m,ith]*m*self.R_ionosphere*dth
					#PSFMA - coefficient transformations for electric potential? to coefficents for
					#Psi, the equivalent current function
					#I_t = r_hat x grad(psi) where I_t = toroidal current, r_hat is paralell to field lines
					PSFMA[i_basisfcn,:] = PSFMA[i_basisfcn,:] + dq_asym[i_basisfcn,ith]*intCur_ph[:,i_m,ith]*self.R_ionosphere*dth*np.sin(theta)
					PSFMA[i_basisfcn,:] = PSFMA[i_basisfcn,:] - q_asym[i_basisfcn,ith]*intCur_th[:,i_neg_m,ith]*self.R_ionosphere*m*dth
				
		#We need values (of PSFMA) for all n for this m
		#to compute
		#ZFMA and VFMA so we
		#must do this loop again
		for i_m,m in enumerate(all_m):
			i_neg_m = len(all_m)-i_m-1 # index of -m
			i_basisfcn_m = range(self.ibm[i_m],self.iem[i_m]+1)
			for i_n_out,i_basisfcn_out in enumerate(i_basisfcn_m):
				# i_p is row of vfmps/zfmps tables
				# nss[i_m] gives row of first n for this m into vfmps/zfmps
				i_p = i_n_out + self.nss[np.abs(m)]
				#Summation over all n for this m
				#print "M=%d,I_N=%d,I_P=%d,ZFMPS=%e" % (m,i_n_out,i_p,self.zfmps[i_p,0])
				for i_n_in,i_basisfcn_in in enumerate(i_basisfcn_m):
					#There's an implicit assumption here: that FAC don't contribute to ground mag perturbations
					#because JFAC = div(Ip) = div(-grad(tau)) and TFMA does not factor in the computation
					#of ZFMA and VFMA 
					ZFMA[i_basisfcn_out,:] = ZFMA[i_basisfcn_out,:] + self.zfmps[i_p,i_n_in]*PSFMA[i_basisfcn_in,:]
					VFMA[i_basisfcn_out,:] = VFMA[i_basisfcn_out,:] + self.vfmps[i_p,i_n_in]*PSFMA[i_basisfcn_in,:]
					#if i_basisfcn_out == 2:
					#	 print "i=%d,M=%d,i_n=%d,VFMA[i,21]=%e,PSFMA[1,21],vfmps=%e,psfma=%e" % (i_basisfcn_in,m,i_n_in,
					#		VFMA[i_basisfcn_out,21],self.vfmps[i_p,i_n_in],PSFMA[i_basisfcn_in,21])

		return ZFMA,VFMA,TFMA,PSFMA

	def get_ground_magnetic_perturbations(self,lats,lons):
		"""

		Get the coefficient for the AMIE basis function
		set which describe expansions of each of the three components
		
			H - ? North
			D - ? East
			Z - ? Down
		
		of magnetic perturbations at the ground as observed by
		ground magnetometers (e.g. SuperMAG)

		"""
		#Get coefficient sets for potential and it's derivatives
		pots,dpotsdph,dpotsdth = self.get_potential(lats,lons,derivatives=True)
		
		#Get sine of colats and earth radius (constants that appear in the coefficient formulae)
	 	sin_theta = np.sin(np.radians(90.-lats))
	 	Ri = self.R_ionosphere

		#Get electric to ground magnetic pertubation AMIE basis function
		#coefficient transformation matrices
		zfma,vfma,tfma,psfma = self.calc_potcoeff_to_groundmagcoeff()

		#Components of the ground magnetic pertrubations
		ZMG = pots
		
		HMG = dpotsdth/Ri*1.0e9
		#Divide through by sine of colatitude
		for col in range(HMG.shape[1]):
			HMG[:,col]/sin_theta

		DMG = -1.*dpotsdph/Ri*1.0e9

		#Formulae from matlab setbase_gmagobs.m, 
		HMG = np.dot(HMG,vfma)
		DMG = np.dot(DMG,vfma)
		ZMG = np.dot(ZMG,zfma)
		
		#To nT
		#HMG,DMG,ZMG = HMG,DMG,ZMG
		return HMG,DMG,ZMG

class EmpericalOrthogonalFunction(object):
	"""
	A class representing a single EOF
	"""
	def __init__(self,EOFSet):
		self.eofmeta = EOFSet
		self.amiebasis = EOFSet.amiebasis
		self.alphas = None
		self.betas = None

class EmpericalOrthogonalFunctionSet(object):
	"""
	A class representing a set of emperical orthogonal functions
	expressed as an expansion of AMIE basis functions
	"""
	def __init__(self,n_eofs=3,n_eig=50):
		self.amiebasis = BasisFunctionSet() #AMIE basis functions class
		self.n_eofs=n_eofs # Use the first n_eofs most dominant eofs in expansions
		self.n_eig=n_eig # Use the first n_eig most dominant eigenvectors of AMIE basis function covariance C_u
		
		#Each beta coefficient for a given EOF is a weight for one of the eigenvectors of C_u
		self.betas = None # [ n_eig x n_eofs ]

		#Each alpha coefficient is a weight for a given EOF for a specific set of observations (which
		#are all associated with a single time) 

		#The alphas are required to express the EOF-based covariance, but only the betas are required
		#to express the EOFs themselves
		self.alphas = None # [n_times x n_eofs]

		#Now we find the eigenvectors of C_u
		self.basis_pc_eigvals,self.basis_pc_eigvecs = self._eof_basis()

	def _cu(self):
		"""
		Loads Richmond and Kamide 1988 Covariance Matrix
		"""
		cov_table_dir = os.path.join(amie_core.tables_dir,'covariance')
		Cu = np.loadtxt(os.path.join(cov_table_dir,'cu_kp3'))
		return Cu

	def eof_basis_weights(index):
		"""
		Get the weight for each AMIE basis function 
		for the index-th EOF 
		"""

		this_eof_betas = self.betas[:,index].reshape((1,-1)) # Make into row vector
		basis_weights = np.dot(this_eof_betas,self.basis_pc_eigvecs)

		return basis_weights

	def _eof_basis(self):
		"""
		Performs Principle Component Analysis on AMIE basis functions,
		via a precomputed covariance matrix C_u which was created for
		the original AMIE procedure

		Returns n_eig eigenvectors associated with the largest n_eig eigenvalues of C_u 
		
		These serve as a basis for expressing each EOF. Since these
		eigenvectors are principal components of the AMIE basis set, they
		automatically contains a large portion of the variability in the AMIE basis functions
		
		Also, since each eigenvector is effectively a set of weights or coefficients for
		the n_basis AMIE basis functions, which we can find values for at an arbitrary location
		r, via the interpolation procedure shown in BasisFunctionSet.get_potential 

		If: 
		  V_k
		  	is the length n_basis eigenvector associated with the k-th largest 
		  	eigenvalue of the covariance matrix C_u ( k in [1,n_eig=50] ),
		  	with components V_k,n
		  	(the coefficient for the n-th AMIE basis function (n in [1,n_basis = 244]))
		  x_n(r) 
		  	is the (scalar) value associated with the n-th AMIE basis function 
		  	evaluated at the location r (n in [1,n_basis = 244])

		Then the weight for the n-th AMIE basis function for the EOF is:
			W_n = sum( beta_k*V_k,n ) where sum runs from k=1 to k=n_eig=50
		
		And the value of the EOF, evaluated at a location r_l, is the linear combination:
			EOF(r) = sum_n( W_n*x_n ) where the sum runs from n=1 to n=n_basis=244

		Note that C_u is n_basis x n_basis and represents the covariance 
		of the basis set
		"""
		n_eig = self.n_eig
		Cu = self._cu()

		#The column v[:, i] is the normalized eigenvector corresponding to the eigenvalue w[i].
		#Since covariance matrices are by definition symmetric we can get eigs with a 
		#specific function (for general matices use np.linalg.eig)
		#Eigen vectors come out in order of INCREASING size, and with degenerate eigenvectors repeated
		w,v = np.linalg.eig(Cu)
		wsort = np.argsort(w)[::-1]

		#i_highest = np.arange(len(w)-1,n_eig-1,-1) #Return in decreasing order (highest eigenvalue first)

		return w[wsort][:n_eig],v[:,wsort][:,:n_eig]

	def load_alpha_beta_from_files(self,alpha_file,beta_file,alpha_juliandays=None):
		"""
		Directly load alpha and beta coefficients from ASCII
		files. This is how alpha and beta are handled in the original
		MATLAB version of AMIE

		both files named by alpha_file and beta_file 
		must have self.n_eofs columns (3 by default)

		alpha_file - str
			valid ASCII filename
			May have any number of rows (weight of each 
			of the 3 EOFs for a particular time)

		beta_file - str
			ASCII filename. Must have self.n_eig rows, each representating the
			weights for each of the 
			n_eig (50 by default) most important PCs of Richmond and Kamide
			covariance matrix Cu

		alpha_juliandays - np.ndarray, optional
			the times (as julian day) for which each alpha was computed
			Must have length == number of rows in alpha_file

		"""
		alphas = np.genfromtxt(alpha_file)
		betas = np.genfromtxt(beta_file)
		#Shapecheck
		if alphas.shape[1] != self.n_eofs:
			raise ValueError('Number of columns in alpha file(%s=%d) does not match number of EOFs(%d)' % (alpha_file,alphas.shape[1],self.n_eofs))
		if betas.shape[1] != self.n_eofs:
			raise ValueError('Number of columns in beta file(%s=%d) does not match number of EOFs(%d)' % (beta_file,betas.shape[1],self.n_eofs))
		if betas.shape[0] != self.n_eig:
			raise ValueError('Number of rows in beta file(%s=%d) does not match number of Cu PCs(%d)' % (beta_file,betas.shape[0],self.n_eig))
		#Store
		self.alphas=alphas
		self.betas=betas

	def plot_eofs(self,filename=None):
		basis = self.amiebasis
		bf_vals = basis.get_potential(basis.lat_grid.flatten(),basis.lon_grid.flatten()) #grid_lats only goes down to ithtrns (transfer colat)
		X,Y = satplottools.latlon2cart(basis.lat_grid.flatten(),basis.lon_grid.flatten(),'N')
		X_grid,Y_grid = X.reshape(basis.lat_grid.shape),Y.reshape(basis.lat_grid.shape)

		f = pp.figure(figsize=(4*(self.n_eofs+1),4))
		for i_eof in range(self.n_eofs):
			ax = pp.subplot(1,self.n_eofs+1,i_eof+1)
			B = np.dot(self.basis_pc_eigvecs,self.betas[:,i_eof])
			eof_grid = np.dot(bf_vals,B).reshape(X_grid.shape)
			satplottools.draw_dialplot(ax)
			ax.contour(X_grid,Y_grid,eof_grid,20,lw=4)
			ax.text(-40.,-40,'Min: %.1f \nMax: %.1f' % (np.nanmin(eof_grid.flatten()),
													np.nanmax(eof_grid.flatten())))
		
		#Bottom Row
		ax = pp.subplot(1,self.n_eofs+1,self.n_eofs+1)
		colors=['r','g','b']
		for i_eof in range(self.n_eofs):
			ax.plot(self.alphas[:,i_eof],colors[i_eof]+'o',label='EOF%d' % (i_eof))
		ax.set_ylabel('Alphas')
		ax.legend()

		pp.subplots_adjust(left=.05,right=.95,top=.95,bottom=.05,hspace=.05,wspace=.05)

		f.savefig('eofs_plot.png' if filename is None else filename)


	def find_alpha_beta_from_data(self,repl_inds,obs,obs_type='perturbations'):
		"""
		Use an iterative process to find the alpha and beta coefficients
		from a total of n_obs observations, which
		are divided into P 'passes',the p-th of which has I_p individual values,
		so n_obs = sum(I_p) over p in [1,P]
		
		As described in: 
		
		Matsuo, Richmod, Nychka, "Modes of high-latitude electric field variablility derived
		from DE-2 measurements: Emperical Orthogonal Function (EOF) analysis", JGR, Vol 29, No 7, 1107,
		DOI 10.1029/2001GL013077, 2002

		repl_inds - n_obs x 1, has P unique integer values
			The index of the 'pass'/ replication each observation is associated with.
			One alpha will be computed for each such replication

		obs - 
			An Observations object, containing n_obs observations

		"""
		
		#Basis function values for observation type	
		if obs_type=='perturbations':
			amiebasis_dBd1,amiebasis_dBd2 = self.amiebasis.get_magnetic_perturbations(obs)
			X_amie = np.row_stack((amiebasis_dBd1,amiebasis_dBd2))
			pcbasis_dBd1 = np.dot(amiebasis_dBd1,self.basis_pc_eigvecs)
			pcbasis_dBd2 = np.dot(amiebasis_dBd2,self.basis_pc_eigvecs)
			X_pc = np.row_stack((pcbasis_dBd1,pcbasis_dBd2))
			#Get Residual Observations
			Y = obs.get_obs_as_matrix() # Raw Observations (all vector components in single matrix)
			beta_mean = amie_core.regress(Y,X_pc) #Find the mean by linear regression against all C_u PCs
			Y_mean = np.dot(X,beta_mean)
			Y_res = Y-Y_mean #Residual observations

		else:
			raise ValueError('Invalid (or not yet implemented) observation type %s' % ( obs_type ) )

		#List of replication indices to iterate over
		repls = np.unique(repl_inds).flatten().tolist()

		#Search for best PC to use for initial alpha estimation
		for pc_ind_alpha in range(len(self.basis_pc_eigvals)):

			Y_alpha = np.zeros_like(Y_res)
			X_beta = np.zeros_like(X_pc)
			Y_alpha.fill(np.nan)
			X_beta.fill(np.nan)
		
			for repl_j in repls:
				in_this_repl = repl_inds == repl_j
				X_j,Y_j = X_pc[in_this_repl,pc_ind_alpha],Y_res[in_this_repl]
				#Estimate Alpha from a single C_u principle component
				#linalg.solve is equivalent in this sense to matlab / (left divide)
				Y_alpha[in_this_repl] = np.linalg.solve(np.dot(X_j.T,X_j),np.dot(X_j.T,Y_j)) 

			for i_pc in range(len(self.basis_pc_eigvals)):
				X_beta[:,i_pc] = Y_alpha*X[:,i_pc]

			#Find coefficients for each PC if we use this alpha
			beta = np.dot(np.linalg.inv(np.dot(X_beta.T,X_beta)),np.dot(X_beta.T,Y))

			#Now reestimate alpha using the full set of C_u principal components
			for repl_j in repls:
				in_this_repl = repl_inds == repl_j
				X_j = np.dot(X_pc[in_this_repl,:],beta)
				Y_j = Y_res[in_this_repl]
				Y_alpha[in_this_repl] = np.linalg.solve(np.dot(X_j.T,X_j),np.dot(X_j.T,Y_j)) 

			#Normalize beta
			beta = beta/sqrt(sum(beta**2))

			#Compute the sum squared error
			s = np.sum((Y - Y_alpha*np.dot(X,beta))**2)

			#Test against previous
			if s > last_s:
				best_pc_ind = pc_ind_alpha
				break

		

import numpy as np
import scipy,os,datetime
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as pp
from matplotlib import pyplot
from sklearn import linear_model #To get equivalent functionality as matlab's regress multivariate regression
from apexpy import apex_converter
from geospacepy import satplottools,special_datetime

#Determine where this module's source file is located
#to determine where to look for the tables
src_file_dir = os.path.dirname(os.path.realpath(__file__))
tables_dir = os.path.join(src_file_dir,'tables')

#These need to know about the tables_dir so import them
#after defining it
import amie_models, amie_basis, amie_obs, amie_cov, amie_taper

#An equivalent utility function to matlab's regress,
#implemented using scikit-learn
def regress(X,y,regression_type='ols'):
	#Ordinary Least Squares regression (probably what MATLAB uses)
	if regression_type=='ols':
		clf = linear_model.LinearRegression()
	else:
		raise ValueError('Regression type %s not recognized!' % (regression_type))
	clf.fit(X,y)
	return clf.coef_

#MLON and MLT interconversion is fairly insensitve 
#to the epoch (only cares about longitude of IGRF magnetic pole)
#so we can fairly safely use a hard-coded epoch
core_apex_converter = apex_converter(epoch=2010.)

def mlon2mlt(jds,mlons,year,converter=None):
	"""
	Convert magnetic (Apex) longtiude to magnetic local time
	"""
	ac = core_apex_converter if converter is None else converter
	flatmlons = mlons.flatten()
	years = year*np.ones_like(jds)
	doys = jds-special_datetime.datetime2jd(datetime.datetime(year,1,1))
	seconds = (doys-np.floor(doys))*86400.
	flatmlts = ac.alon2mlt(flatmlons,years,doys,seconds)
	return flatmlts.reshape(mlons.shape)

def mlt2mlon(jds,mlts,year,converter=None):
	"""
	Convert magnetic local time to magnetic (Apex) longitude
	"""
	ac = core_apex_converter if converter is None else converter
	flatmlts = mlts.flatten()
	years = year*np.ones_like(jds)
	doys = jds-special_datetime.datetime2jd(datetime.datetime(year,1,1))
	seconds = (doys-np.floor(doys))*86400.
	flatmlons = ac.mlt2alon(flatmlts,years,doys,seconds)
	return flatmlons.reshape(mlts.shape)

class AmieLocations(object):
	"""
	A class that simply represents a collection
	of locations. E.G. a grid of lat,lon
	or a bunch of observation locations.

	Handles observation location transforms
	and getting apex geometric factors and apex coord transforms
	"""
	def __init__(self,lats,azi,azi_lonorlt='lt'):
		self.lats = lats
		self.lons = lons
		self.mlts = lts

class AmieAssimilationResult(object):
	"""
	A single assimilation result
	"""
	def __init__(self,dt,startdt=None,enddt=None,hemi='N',basisset=None,covariance_model=None,background_model=None,conductance_model=None):
		#Set the time (center time of the assimilation) and hemisphere
		self.dt,self.hemi = dt,hemi
		self.hemisign = 1. if hemi is 'N' else -1.

		#Default to +-1 day around the center time for the omni data
		if startdt is None:
			startdt = datetime.datetime(self.dt.year,self.dt.month,self.dt.day)-datetime.timedelta(days=1)
		if enddt is None:
			enddt = datetime.datetime(self.dt.year,self.dt.month,self.dt.day)+datetime.timedelta(days=2)
		
		#Load the OvationPyme diffuse aurora conductance model if no other conductance model is specified
		self.conductance_model = conductance_model if conductance_model is not None else amie_models.OvationPymeConductance(startdt,enddt)
		self.conductance_model.dt = self.dt

		#Get the basis function set we will use
		self.basisset = basisset if basisset is not None else amie_basis.BasisFunctionSet(conductance_model=self.conductance_model)

		#Load the original AMIE covariance matrix if no other covariance model is specified
		self.covariance_model = covariance_model if covariance_model is not None else amie_cov.AmieCuKp3Covariance()

		#Create the output grid. By default use the 'built-in' grid used to create the AMIE basis functions
		#self.lat_grid, self.lon_grid = np.meshgrid(self.basisset.lats,self.basisset.lons,indexing='ij')
 		self.lat_grid, self.lon_grid = self.basisset.lat_grid,self.basisset.lon_grid

		#Output locations in cartesian centered on the pole (for plotting)
		self.X,self.Y = satplottools.latlon2cart(self.lat_grid.flatten(),self.lon_grid.flatten(),self.hemi)
		self.X_grid,self.Y_grid = self.X.reshape(self.lat_grid.shape),self.Y.reshape(self.lat_grid.shape)

		#Load the background model and calculate the background model expansion in basis functions
		self.background_model = background_model if background_model is not None else amie_models.CS10ElectricPotential(startdt,enddt)
		self.background_model.dt = self.dt
		self.model_pot = self.background_model.get_potential(self.lat_grid.flatten(),self.lon_grid.flatten())

		#Compute the AMIE basis function weights for potential (electric?) at the grid locations
		self.pot = self.basisset.get_potential(self.lat_grid.flatten(),self.lon_grid.flatten())

		#Linearly regress the model potential across the basis functions (x_prior has len n_basis_functions)
		self.x_prior = regress(self.pot,self.model_pot)

		#Create lists which when concatenated together will be the lat and lon of the observations
		self.y_lat_list,self.y_lon_list = [],[]

		#Lists which will be concatenated together to make:
		# y - the observations vector
		#		dimension = n_observations x 1
		# H - the 'forward operator' i.e. the matrix which when multiplyied by the background model values 'x'
		#	to yield the weight for each basis functions, which can be summed to get back the model value
		#	dimension = n_grid_locations x n_basis_functions
		# Cr_diag - the diagonal of the representativeness / observation error covariance matrix (i.e. ith element 
		#			of Cr_diag is the variance associated with the i-th observation) 
		#			dimension = n_observations x 1
		# These are lists so that observations can be ingested in several sequential steps (i.e.
		# ground mags, then radars, etc.)
		# They will be concatenated together when the assimilate method is called
		self.y_list,self.H_list,self.Cr_diag_list = [],[],[]		

	def ingest(self,observations):
		"""
		Ingests the observations contained in the AmieObservations subclass observations
		"""
		
		#Get observation locations
		lats,lons,y,y_var,H = observations.get_ingest_data()
		self.y_lat_list.append(lats)
		self.y_lon_list.append(lons)

		#y, observation values and y_var, associated variances
		#we assume uncorrellated errors between observations
		self.y_list.append(y)
		self.Cr_diag_list.append(y_var)

		#H, the basis function coefficients for each observation location 
		self.H_list.append(H)

	def validate(self,observations,prior=False,residuals=False,localize=True):
		"""
		Check a solution against a set of observations
		If the observations were not used in the assimilation
		this is cross validation. If they were this is observation-minus-forecast residuals.

		Localizes the observations first
		"""
		validation_set = not residuals # residuals==True means 'validate' using data include in assimilation
		lats,lons,y,y_var,H = observations.get_ingest_data(validation_set=validation_set)
		Cr = np.diag(y_var)

		if not prior and localize:
			#Compute COR - obs-obs
			theta_obs = np.radians(90.-lats).flatten()
			phi_obs = np.radians(lons).flatten()
			COR = amie_taper.GaspariCohnCorrelationPolar(theta_obs,phi_obs,theta_obs,phi_obs,self.c)
			H_Cb_HT  = COR*np.dot(H,np.dot(self.Cb,H.T))  #localized H*COV*H'

			#Calculate the denominator for the gain matrix
			innerK = np.linalg.inv(H_Cb_HT + Cr)
			K = np.dot(H_Cb_HT,innerK)

			predicted_y = np.dot(H,self.x_prior) + np.dot(K , (y-np.dot(H , self.x_prior)) )
			
			deviations = y-predicted_y

		elif not prior and not localize:

			deviations = y - np.dot(H,self.x_posterior)

		else:
			
			deviations = y - np.dot(H,self.x_prior)

		return lats,lons,deviations

	def covariance_balance(self,observations,localize=True):
		"""
		Check to what degree eq. 17, covariance of the observations-minus-forcast residuals
		in Dee and Silva, 1998, is satistified, assuming that the cross-covariance of the 
		observations with the model is zero:

		(17) < (v-<v>)*(v-<v>).T > = R - H*P*H

		Where, in our case, R = C_r, P = C_b, and H is still the forward operator transforming
		an expansion of the assimilation variable in amie_basis functions on the model grid
		into an expansion of the observation variable (magnetic perturbations, 
		l.o.s velocity, etc.) at observation locations.

		Dee and Silva, Maximum-Likelihood Estimation of Forecast and Observation Error Covariance
		Parameters. Part I: Methodology, Monthly Weather Review, 1998

		eq. 17 and 18
		"""
		lats,lons,omf = self.validate(observation,prior=False,residuals=True,localize=localize)
		#OMF is observation-minus-forecast

		lhs = np.cov(omf.flatten()) #Sample covariance of observations-minus-forecast

		if localize:
			#Compute COR - obs-obs
			theta_obs = np.radians(90.-lats).flatten()
			phi_obs = np.radians(lons).flatten()
			COR = amie_taper.GaspariCohnCorrelationPolar(theta_obs,phi_obs,theta_obs,phi_obs,self.c)
			H_Cb_HT  = COR*np.dot(H,np.dot(self.Cb,H.T))  #localized H*COV*H'

		else:

			H_Cb_HT = np.dot(H,np.dot(self.Cb,H.T))

		rhs = self.Cr - H_Cb_HT

		return lats,lons,np.diag((lhs-rhs))

	def prepare(self):
		"""
		Concatenates all ingested observations and stores them to instance variables
		"""

		self.y = np.concatenate(self.y_list,axis=0)
		self.ylats = np.concatenate(self.y_lat_list,axis=0)
		self.ylons = np.concatenate(self.y_lon_list,axis=0)

		self.H = np.concatenate(self.H_list,axis=0)
		self.Cr = np.diag(np.concatenate(self.Cr_diag_list,axis=0))
		self.Cb = self.covariance_model.prior_model_covariance

		return self.y,self.ylats,self.ylons,self.H,self.Cr,self.Cb

	def localized_assimilate(self,c=None):
		"""
		Fully localized assimilation (as in amie matlab ampere)
		"""
		if c is None:
			c = 6.481e6*np.pi/10 #18-36 deg

		y,ylats,ylons,H,Cr,Cb = self.prepare()

		#Compute COR - obs-obs
		theta_obs = np.radians(90.-self.ylats).flatten()
		phi_obs = np.radians(self.ylons).flatten()
		COR = amie_taper.GaspariCohnCorrelationPolar(theta_obs,phi_obs,theta_obs,phi_obs,c)
		H_Cb_HT  = COR*np.dot(H,np.dot(Cb,H.T))  #localized H*COV*H'

		#Calculate the denominator for the gain matrix
		innerK = np.linalg.inv(H_Cb_HT + Cr)
		
		#Compute COR - grid-obs
		theta_grid = np.radians(90.-self.lat_grid).flatten()
		phi_grid = np.radians(self.lon_grid).flatten()
		COR = amie_taper.GaspariCohnCorrelationPolar(theta_grid,phi_grid,theta_obs,phi_obs,c)
		gridpot_Cb_HT = COR*np.dot(np.dot(self.pot,Cb),H.T)  #localized grid_pot*COV*H'
		H_Cb_gridpot = COR.T*np.dot(np.dot(self.H,Cb),self.pot.T) #For predicting observations
		
		
		#Finish computing gain (numerator Cb*H.T is localized grid-obs, 
		#denominator H*Cb*H.T is localized obs-obs)
		K = np.dot(gridpot_Cb_HT,innerK)

		#Compute COR - grid-grid
		COR = amie_taper.GaspariCohnCorrelationPolar(theta_grid,phi_grid,theta_grid,phi_grid,c)
		
		gridpot_Cb_gridpot = COR*np.dot(np.dot(self.pot,Cb),self.pot.T)  #localized grid_pot*COV*grid_pot'

		#Prior model state is weight of each basis function 
		# from regression of potential from the background model
		# against basis function values for potential
		x_prior = self.x_prior

		#Perform the update step
		#self.x_posterior = x_prior + np.dot(K , (y-np.dot(H , x_prior)) )

		#Update the covariance (get analysis error covariance)
		#Don't know if this actually is appropriate for localized case, 
		#just copy pasted it from unlocalized func...
		#Ca = np.dot( np.eye(len(x_prior))-np.dot(K,H) , Cb )

		#Localized Analysis Error
		self.analysis_error_covariance = gridpot_Cb_gridpot-np.dot(K,gridpot_Cb_HT.T)
		self.analysis_error = np.sqrt(np.diag(self.analysis_error_covariance))

		#Get the predicted potential
		self.predicted_pot = np.dot(self.pot,self.x_prior) + np.dot(K , (y-np.dot(H , x_prior)))

		#Get the predicted observations
		
		#Get prior error
		self.prior_model_error_covariance = gridpot_Cb_gridpot
		self.prior_model_error = np.sqrt(np.diag(self.prior_model_error_covariance))
		
		#For debugging
		self.K = K
		self.c = c

		#  %Mag potential
		#fit_weimer_pot_grid = grid_pot*beta; %Model potential on grid
		#fit_X1_pot_grid = fit_weimer_pot_grid + gridpotPH/(HPH+R)*(Y-Hx);     %grid_pot*X1, OI update to model pot
		#%FAC 
		#fit_weimer_fac = grid_FAC*beta; %Model FAC on grid
		#fit_X1_fac_grid = fit_weimer_fac + gridfacPH/(HPH+R)*(Y-Hx);          %grid_FAC*X1, OI update to model FAC

		#Compute Analysis Error
		# This is equation 14 in paper, Aerr = C_a
		#Aerr = diag(gridpotP - gridpotPH/(HPH+R)*gridpotPH');


	def assimilate(self):
		"""
		Actually calculate the gain matrix and do the assimilation
		"""
		y,ylats,ylons,H,Cr,Cb = self.prepare()

		print "y.shape=%s, n_nan=%d" % (str(y.shape),np.count_nonzero(np.isnan(y)))
		print "H.shape=%s, n_nan=%d" % (str(H.shape),np.count_nonzero(np.isnan(H)))
		print "Cr.shape=%s, n_nan=%d" % (str(Cr.shape),np.count_nonzero(np.isnan(Cr)))
		print "Cb.shape=%s, n_nan=%d" % (str(Cb.shape),np.count_nonzero(np.isnan(Cb)))

		#Calculate the gain matrix
		innerK = np.linalg.inv(np.dot(H,np.dot(Cb,H.T)) + Cr)
		K = np.dot(Cb,np.dot(H.T,innerK))

		#Prior model state is weight of each basis function 
		# from regression of potential from the background model
		# against basis function values for potential
		x_prior = self.x_prior
		print np.count_nonzero(np.isnan(self.x_prior))

		#Perform the update step
		x_posterior = x_prior + np.dot(K , (y-np.dot(H , x_prior)) )

		#Update the covariance (get analysis error covariance)
		Ca = np.dot( np.eye(len(x_prior))-np.dot(K,H) , Cb )
		print "Ca.shape=%s" % (str(Ca.shape))

		#Set the instance variables
		self.x_posterior = x_posterior
		
		#Get the predicted potential
		self.predicted_pot = np.dot(self.pot,self.x_posterior)
		self.analysis_error_covariance = np.dot(np.dot(self.pot,Ca),self.pot.T)
		self.analysis_error = np.sqrt(np.diag(self.analysis_error_covariance))
		self.prior_model_error_covariance = np.dot(np.dot(self.pot,Cb),self.pot.T)
		self.prior_model_error = np.sqrt(np.diag(self.prior_model_error_covariance))
		print 'analysis_error.shape=%s' % (str(self.analysis_error.shape))
		print 'prior_model_error.shape=%s' % (str(self.prior_model_error.shape))

		#For debugging
		self.K = K
		self.Ca = Ca

	def pot2fac(self,pot):
		"""
		Do (spherical harmonic not Apex) horizontal laplacian on amie basis function representation
		of potential
		"""
		fac = pot.copy()
		mu0 = 4*np.pi*1.0e-7
		r_ionosphere = (6371.2+110.)*1000.
		for i_loc in range(self.pot.shape[0]):
			fac[i_loc,:] = -1.0*1.0e4/mu0/r_ionosphere**2*self.pot[i_loc,:]*self.basisset.nnp1.flatten()
		return fac

	def contour_model_potential(self,ax,vmin=-30000.,vmax=30000.,cmap=None,**kwargs):
		"""
		Make a contour plot of the model potential
		on Axes ax
		"""
		cmap_pot = pp.cm.get_cmap('jet') if cmap is None else cmap
		norm = mpl.colors.Normalize(vmin=vmin,vmax=vmax)
		model_pot = self.model_pot.reshape(self.X_grid.shape)
		levels = np.linspace(vmin,vmax,20)
		mappable = ax.contour(self.X_grid,self.Y_grid,model_pot,
								20,levels=levels,cmap=cmap_pot,alpha=.7,norm=norm,lw=5,**kwargs)
		ax.text(-40.,-40,'Min: %.3e \nMax: %.3e' % (np.nanmin(model_pot.flatten()),
													np.nanmax(model_pot.flatten())))
		
		return mappable

	def contour_model_fac(self,ax,vmin=-5.,vmax=5.,cmap=None,**kwargs):
		"""
		Make a contour plot of the model field aligned currents
		on Axes ax
		"""
		cmap_pot = pp.cm.get_cmap('seismic') if cmap is None else cmap
		norm = mpl.colors.Normalize(vmin=vmin,vmax=vmax)
		model_fac = np.dot(self.pot2fac(self.pot),self.x_prior).reshape(self.X_grid.shape)
		mappable = ax.contour(self.X_grid,self.Y_grid,model_fac,
								20,cmap=cmap_pot,alpha=.7,norm=norm,lw=5,**kwargs)
		ax.text(-40.,-40,'Min: %.3e \nMax: %.3e' % (np.nanmin(model_fac.flatten()),
													np.nanmax(model_fac.flatten())))
		
		return mappable

	def contour_innovation_potential(self,ax,vmin=-500.,vmax=500.,cmap=None,**kwargs):
		"""
		Make a contour plot of the model potential
		on Axes ax
		"""
		cmap_pot = pp.cm.get_cmap('jet') if cmap is None else cmap
		model_pot = self.model_pot.reshape(self.X_grid.shape)
		predicted_pot = self.predicted_pot.reshape(self.X_grid.shape)
		innovation = predicted_pot-model_pot
		innovation = innovation-np.nanmean(innovation.flatten())
		vmin,vmax = np.nanmin(innovation.flatten()),np.nanmax(innovation.flatten())
		levels = np.linspace(vmin,vmax,20)
		norm = mpl.colors.Normalize(vmin=vmin,vmax=vmax)
		mappable = ax.contour(self.X_grid,self.Y_grid,innovation,
								20,levels=levels,cmap=cmap_pot,alpha=.7,norm=norm,lw=5,**kwargs)
		ax.text(-40.,-40,'Min: %.3e \nMax: %.3e' % (np.nanmin(innovation.flatten()),
													np.nanmax(innovation.flatten())))
		return mappable
	

	def contour_predicted_potential(self,ax,vmin=-30000.,vmax=30000.,cmap=None,**kwargs):
		"""
		Make a contour plot of the predicted potential
		on Axes ax
		"""
		cmap_pot = pp.cm.get_cmap('jet') if cmap is None else cmap
		norm = mpl.colors.Normalize(vmin=vmin,vmax=vmax)
		predicted_pot = self.predicted_pot.reshape(self.X_grid.shape)
		levels = np.linspace(vmin,vmax,20)
		mappable = ax.contour(self.X_grid,self.Y_grid,predicted_pot,
								20,levels=levels,cmap=cmap_pot,alpha=.7,norm=norm,lw=5,**kwargs)
		ax.text(-40.,-40,'Min: %.3e \nMax: %.3e' % (np.nanmin(predicted_pot.flatten()),
													np.nanmax(predicted_pot.flatten())))
		return mappable

	def contour_predicted_fac(self,ax,vmin=-10.,vmax=10.,cmap=None,**kwargs):
		"""
		Make a contour plot of the predicted potential
		on Axes ax
		"""
		cmap_pot = pp.cm.get_cmap('seismic') if cmap is None else cmap
		norm = mpl.colors.Normalize(vmin=vmin,vmax=vmax)
		predicted_fac = np.dot(self.pot2fac(self.pot),self.x_posterior).reshape(self.X_grid.shape)
		mappable = ax.contour(self.X_grid,self.Y_grid,predicted_fac,
								20,cmap=cmap_pot,alpha=.7,norm=norm,lw=5,**kwargs)
		ax.text(-40.,-40,'Min: %.3e \nMax: %.3e' % (np.nanmin(predicted_fac.flatten()),
													np.nanmax(predicted_fac.flatten())))
		return mappable

	def contour_analysis_error(self,ax,vmin=None,vmax=None,cmap=None,use_fac=False,**kwargs):
		"""
		Make a contour plot of the predicted potential
		on Axes ax
		"""
		cmap_err = pp.cm.get_cmap('jet') if cmap is None else cmap
		if not use_fac:
			error = self.analysis_error.reshape(self.X_grid.shape)
		else:
			fac = self.pot2fac(self.pot)
			analysis_error_covariance = np.dot(np.dot(fac,self.Ca),fac.T)
			analysis_error = np.sqrt(np.diag(analysis_error_covariance))
			error = analysis_error.reshape(self.X_grid.shape)
		if vmin is None or vmax is None:
			vmin,vmax = np.nanmin(error.flatten()),np.nanmax(error.flatten())
		norm = mpl.colors.Normalize(vmin=vmin,vmax=vmax)
		mappable = ax.contour(self.X_grid,self.Y_grid,error,
								20,cmap=cmap_err,alpha=.7,norm=norm,lw=5,**kwargs)
		ax.text(-40.,-40,'Min: %.3e \nMax: %.3e' % (np.nanmin(error.flatten()),np.nanmax(error.flatten())))
		return mappable

	def contour_error_innovation(self,ax,vmin=None,vmax=None,cmap=None,use_fac=False,**kwargs):
		"""
		Make a contour plot of the predicted potential
		on Axes ax
		"""
		cmap_err = pp.cm.get_cmap('jet') if cmap is None else cmap
		if not use_fac:
			prior_error = self.prior_model_error.reshape(self.X_grid.shape)
		else:
			fac = self.pot2fac(self.pot)
			model_error_covariance = np.dot(np.dot(fac,self.Cb),fac.T)
			model_error = np.sqrt(np.diag(model_error_covariance))
			prior_error = model_error.reshape(self.X_grid.shape)
		#posterior_error = self.analysis_error.reshape(self.X_grid.shape)
		error = prior_error
		if vmin is None or vmax is None:
			vmin,vmax = np.nanmin(error.flatten()),np.nanmax(error.flatten())
		
		norm = mpl.colors.Normalize(vmin=vmin,vmax=vmax)
		mappable = ax.contour(self.X_grid,self.Y_grid,error,
								20,cmap=cmap_err,alpha=.7,norm=norm,lw=5,**kwargs)
		ax.text(-40.,-40,'Min: %.3e \nMax: %.3e' % (np.nanmin(error.flatten()),np.nanmax(error.flatten())))
		
		return mappable

	def overplot_observation_locations(self,ax,s=4,validation_set=False,**kwargs):
		"""
		(scatter)plot the observation locations as dots on a polar plot
		"""
		X,Y = satplottools.latlon2cart(self.ylats,self.ylons,self.hemi)
		ax.scatter(X,Y,s=s,c='w',alpha=.5,**kwargs)

def ampere_simple():
	"""
	Test ability to ingest and assimilate ampere data
	"""

	f = pp.figure(figsize=(8,8))
	a1 = f.add_subplot(221)
	a2 = f.add_subplot(222)
	a3 = f.add_subplot(223)
	a4 = f.add_subplot(224)
	pp.subplots_adjust(left=.05,right=.95,top=.95,bottom=.05,hspace=.05,wspace=.05)
	all_ax = [a1,a2,a3,a4]

	daydt = datetime.datetime(2010,5,29,0,0,0)
	om_startdt,om_enddt = datetime.datetime(2010,5,28,0,0,0),datetime.datetime(2010,5,30,0,0,0)

	ampncdf = '/media/liamk/expansiontux/archive/AMPERE/new/invert/2010/20100529Amp_invert.ncdf'
	weimer_dir = '/home/liamk/mirror/Projects/amiepy/amiematlabampere/magdata/20100529_4min/'
	eof_dir = '/home/liamk/mirror/Projects/amiepy/amiematlabampere/trange4_ver2_20100529/'
	#Load AMPERE data

	amp = amie_obs.Ampere(daydt,ampncdf)
	basisset = amie_basis.BasisFunctionSet(conductance_model=None) # Don't need conductance
	conductance_model = amie_models.OvationPymeConductance(om_startdt,om_enddt,mlon_is_mlt_in_degrees=True)
	
	startmin,endmin,centermin = 10,14,12
	last_endmin = 300
	step_min = 2

	result_num = 0 
	localize = True
	while endmin < last_endmin:
		startdt,enddt = daydt+datetime.timedelta(minutes=startmin),daydt+datetime.timedelta(minutes=endmin)
		dt = daydt+datetime.timedelta(minutes=centermin)
		startjd,endjd = special_datetime.datetime2jd(startdt),special_datetime.datetime2jd(enddt)

		alphas_table = os.path.join(eof_dir,'alpha_all_%d.dat' % (centermin))
		betas_table = os.path.join(eof_dir,'beta_all_%d.dat' % (centermin))
		weimer_table = os.path.join(weimer_dir,'weimer_%.4d_%.4d.dat' % (startmin,endmin))
	
		#Table based covariance model and background values
		background_model = amie_models.PotentialTable(weimer_table,basisset=basisset)
		covariance_model = amie_cov.EOFBasedCovariance(alphas_table,betas_table)
		#covariance_model.EOFbasis.plot_eofs(filename='eofplots_%d.png' % (centermin))
 		
		for a in all_ax:
			a.cla()
			satplottools.draw_dialplot(a)

		#Create an assimilator class (we will let it auto-intialize the electric potential and 
		# conductance model, though we could pass them manually)
		amie_asim = AmieAssimilationResult(dt,hemi='N',startdt=om_startdt,enddt=om_enddt,
											conductance_model=conductance_model,
											background_model=background_model,
											covariance_model=covariance_model,
											basisset=basisset)

		#Test that we can get the ingest data
		amp.startdt,amp.enddt = startdt,enddt
		amp.hemi = 'N'
		lats,lons,y,y_var,H = amp.get_ingest_data()
		print("Mean Absolute Observation: %f nT" % (np.nanmean(np.abs(y))))

		#Test that the ingest method works as intended
		amie_asim.ingest(amp)

		#Test that we can actually do the assimilation
		if not localize:
			amie_asim.assimilate()
		else:
			amie_asim.localized_assimilate()

		#Cross Validate
		xv_lats,xv_lons,xv_deviation = amie_asim.validate(amp,localize=localize)
		xv_lats,xv_lons,xv_deviation_prior = amie_asim.validate(amp,prior=True,localize=localize)
		v_lats,v_lons,v_deviation = amie_asim.validate(amp,residuals=True,localize=localize)
		
		i_xobs = xv_deviation.shape[0]/2
		xv_lats,xv_lons = xv_lats[:i_xobs],xv_lons[:i_xobs]
		xv_Bd1, xv_Bd2 = xv_deviation[:i_xobs],xv_deviation[i_xobs:]
		xv_Bd1_p, xv_Bd2_p = xv_deviation_prior[:i_xobs],xv_deviation_prior[i_xobs:]
		
		i_obs = v_deviation.shape[0]/2
		v_Bd1, v_Bd2 = v_deviation[:i_obs],v_deviation[i_obs:]
		
		xv_B = np.sqrt(xv_Bd1**2+xv_Bd2**2)
		xv_B_p = np.sqrt(xv_Bd1_p**2+xv_Bd2_p**2)
		v_B = np.sqrt(v_Bd1**2+v_Bd2**2)
		
		xv_X,xv_Y = satplottools.latlon2cart(xv_lats,xv_lons,amp.hemi)

		#Plot results
		minpot,maxpot = -40.,40.
		minfac,maxfac = -1.,1.
		
		#amie_asim.contour_model_fac(a1,vmin=minfac,vmax=maxfac)
		amie_asim.contour_model_potential(a1,vmin=minpot,vmax=maxpot)
		#a1.scatter(xv_X,xv_Y,xv_B_p/10.,color='m',alpha=.6)
		#amp.plot_expected_vectors(a1,amie_asim.x_prior,H)
		a1.set_title('Weimer [Prior] Potential')
		#a1.text(-40.,40.,'Avg Xval: %.1fnT' % (np.nanmean(xv_B_p)))
		
		#amie_asim.contour_predicted_fac(a2,vmin=minfac,vmax=maxfac)
		amie_asim.contour_predicted_potential(a2,vmin=minpot,vmax=maxpot,zorder=-10)
		#amp.plot_vectors(a2)
		a2.scatter(xv_X,xv_Y,xv_B/10.,color='k',alpha=.6)
		a2.set_title('AMIE [Posterior] Potential')
		a2.text(-40.,40.,'WEIM Res: %.1fnT' % (np.nanmean(xv_B_p)))
		a2.text(-40.,35.,'AMIE Res: %.1fnT' % (np.nanmean(v_B)))
		a2.text(-40.,30.,'AMIE Val Res: %.1fnT' % (np.nanmean(xv_B)))
		
		amie_asim.contour_error_innovation(a3,vmin=0.,vmax=20.,use_fac=False)
		a3.set_title('Model [Prior] Error')
		
		amie_asim.contour_analysis_error(a4,vmin=0.,vmax=20.,use_fac=False)

		#amp.plot_vectors(a3)
		amie_asim.overplot_observation_locations(a4,color='w')
		a4.set_title('Prediction [Posterior] Error')
		
		f.suptitle('%s-%s' % (startdt.strftime('%H:%M'),enddt.strftime('%H:%M')) )
		f.savefig(os.path.join(src_file_dir,'amie_test_ampere_%.3d.png' % (result_num)))

		startmin += step_min
		endmin += step_min
		centermin += step_min
		result_num += 1

def supermag_simple():
	"""
	Test ability to ingest and assimilate supermag
	"""
	#Prepare plot
	f = pp.figure(figsize=(8,8))
	a1 = f.add_subplot(321)
	a2 = f.add_subplot(322)
	a3 = f.add_subplot(323)
	a4 = f.add_subplot(324)	
	a5 = f.add_subplot(325)
	a6 = f.add_subplot(326)
	all_ax = [a1,a2,a3,a4,a5,a6]
	
	#Range of times for omniwed data for input to conductance and electric potential
	#models
	om_startdt,om_enddt = datetime.datetime(2010,5,28,0,0,0),datetime.datetime(2010,6,1,0,0,0)

	#Pre-create the conductance model, electric potential model, and basis set objects
	conductance_model = amie_models.OvationPymeConductance(om_startdt,om_enddt,mlon_is_mlt_in_degrees=True)
	background_model = amie_models.CS10ElectricPotential(om_startdt,om_enddt,mlon_is_mlt_in_degrees=True)
	basisset = amie_basis.BasisFunctionSet(conductance_model=conductance_model)
	#covariance_model = amie_cov.AmieCuKp3Covariance()
 	covariance_model = amie_cov.CS10Covariance()
 
	#Load one day of SuperMAG test data
	fn = os.path.join(tables_dir,'test_amie_obs','20160914-20-23-supermag.txt')
	sm = amie_obs.SuperMAG(supermag_file=fn,basisset=basisset,mlon_is_mlt_in_degrees=True)
	sm.hemi = 'N' #Use northern hemisphere data
	
	startdt = datetime.datetime(2010,5,29,20,8,0)
	enddt = datetime.datetime(2010,5,29,20,12,0)
			
	t_width = 4 # Assimilation window in minutes
	t_step = 2 # Assimilation step size in minutes
	result_num = 0 #Counter for results
	lastdt = datetime.datetime(2010,5,29,22,0,0)
	localize = True
	while enddt < lastdt:
		for a in all_ax:
			a.cla()
			satplottools.draw_dialplot(a)
		

		#Four minute window centered on noon of 2010-5-29
		startjd = special_datetime.datetime2jd(startdt)
		endjd = special_datetime.datetime2jd(enddt)	
		dt = startdt + datetime.timedelta(minutes=t_width/2)
		sm.startdt,sm.enddt = startdt,enddt
	
		#Create an assimilator class (we will let it auto-intialize the electric potential and 
		# conductance model, though we could pass them manually)
		amie_asim = AmieAssimilationResult(dt,hemi='N',startdt=om_startdt,enddt=om_enddt,
											conductance_model=conductance_model,
											background_model=background_model,
											covariance_model=covariance_model,
											basisset=basisset)	

		#Test that we can get the ingest data
		lats,lons,y,y_var,H = sm.get_ingest_data()

		#Load the conductance data used in determining H for
		#debugging plots
		cond_lat,cond_lon = sm.basisset.cond_lat,sm.basisset.cond_lon
		cond_X,cond_Y = satplottools.latlon2cart(cond_lat.flatten(),cond_lon.flatten(),'N')
		cond_X = cond_X.reshape(cond_lat.shape)
		cond_Y = cond_Y.reshape(cond_lon.shape)
		cond_ped,cond_hall = sm.basisset.cond_ped,sm.basisset.cond_hall

		#Test that the ingest method works as intended
		amie_asim.ingest(sm)

		#Test that we can actually do the assimilation
		if localize:
			amie_asim.localized_assimilate()
		else:
			amie_asim.assimilate()

		#Residuals of data not included in assimilation (cross validate)
		xv_lats,xv_lons,xv_deviation = amie_asim.validate(sm,localize=localize)
		xv_lats,xv_lons,xv_B = xv_lats[:len(xv_lats)/3],xv_lons[:len(xv_lats)/3],xv_deviation.reshape((len(xv_lats)/3,3))
		xv_B = np.sqrt(xv_B[:,0]**2+xv_B[:,1]**2+xv_B[:,2]**2)

		#dum1,dum2,xv_deviation_prior = amie_asim.validate(sm,prior=True,localize=localize)
		#xv_B_prior = xv_deviation_prior.reshape((len(xv_lats)/3,3))
		#xv_B_prior = np.sqrt(xv_B_prior[:,0]**2+xv_B_prior[:,1]**2+xv_B_prior[:,2]**2)
		
		xv_X,xv_Y = satplottools.latlon2cart(xv_lats,xv_lons,amie_asim.hemi)
		
		#Residuals of data included in assimilation (validation)
		v_lats,v_lons,v_deviation = amie_asim.validate(sm,residuals=True,localize=localize)
		v_lats,v_lons,v_B = v_lats[:len(v_lats)/3],v_lons[:len(v_lats)/3],v_deviation.reshape((len(v_lats)/3,3))
		v_B = np.sqrt(v_B[:,0]**2+v_B[:,1]**2+v_B[:,2]**2)
		v_X,v_Y = satplottools.latlon2cart(v_lats,v_lons,amie_asim.hemi)
		
		#Plot results
		a1.contourf(cond_X,cond_Y,cond_ped,30,vmin=0.,vmax=15.)
		a1.set_title('Pedersen Conductance')
		a2.contourf(cond_X,cond_Y,cond_hall,30,vmin=0.,vmax=20.)
		a2.set_title('Hall Conductance')
		amie_asim.contour_model_potential(a3)
		sm.plot_expected_vectors(a3,amie_asim.x_prior,amie_asim.H)
		#a3.text(-40.,40.,'Avg Prior Xval: %.1fnT' % (np.nanmean(xv_B_prior)),color='red')
		
		a3.set_title('CS10 Potential')
		#amie_asim.contour_innovation_potential(a3)
		#a3.set_title('AMIE-CS10 Potential')
		amie_asim.contour_predicted_potential(a4)
		a4.text(-40.,40.,'Avg Val: %.1fnT' % (np.nanmean(xv_B)),color='red')
		a4.text(-40.,33.,'Avg Res: %.1fnT' % (np.nanmean(v_B)),color='black')

		a4.set_title('Predicted Potential')
		amie_asim.contour_analysis_error(a5)
		a5.set_title('Error in Potential')
		amie_asim.overplot_observation_locations(a5,color='w')
		a5.scatter(xv_X,xv_Y,8,'r')
		a5.scatter(v_X,v_Y,8,'k')
		
		amie_asim.contour_error_innovation(a6)
		a6.set_title('Error in Potential - Model Error')
		sm.plot_vectors(a4,'N')
		sm.plot_vectors(a5,'N')

		f.suptitle('%s-%s' % (startdt.strftime('%H:%M'),enddt.strftime('%H:%M')) )
		f.savefig(os.path.join(src_file_dir,'amie_test_%.3d.png' % (result_num)))

		startdt += datetime.timedelta(minutes=t_step)
		enddt += datetime.timedelta(minutes=t_step)
		result_num += 1

if __name__ == '__main__':
	ampere_simple()
	#supermag_simple()
	
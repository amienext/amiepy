import numpy as np
import scipy,os
import amie_core, amie_basis

#Library of models (conductance, electric potential, etc.)
import just_sam
import ovationpyme
from ovationpyme import ovation_prime
from geospacepy import special_datetime

class PotentialTable(object):
	"""
	The simplest case. This is just a thin
	wrapper which loads potential values from a table
	at the grid locations for the associated basis set

	Note that expects longtiude to vary across rows in table
	and latitude across columns
	"""
	def __init__(self,tablefile,basisset=None):
		self.pot = np.genfromtxt(tablefile) #Wiemer dataset is 37 x 25
		self.basisset = amie_basis.BasisFunctionSet() if basisset is None else basisset
		#
		n_mlats = self.basisset.lat_grid.shape[0]
		n_mlons = self.basisset.lon_grid.shape[1]

		print "Table shape %s, expected shape %d lats, %d lons" % (str(self.pot.shape),n_mlats,n_mlons)

		if self.pot.shape[0] == n_mlons+1:
			#self.pot = (self.pot[1:,1:].T+self.pot[:-1,:-1].T)/2 #Cut off a row and column to make dimensions match :-(
			self.pot = self.pot[1:,1:].T #Cut off a row and column to make dimensions match :-(

		if self.pot.shape[0] != n_mlats:
			raise ValueError('Shape of potential array from file %s is wrong (expected %d rows(lats), got %d)!' % (tablefile,
				n_mlats,self.pot.shape[0]))

		# 90-transfer latitude (where basis function become trig not spherical harm)
		self.mlats = self.basisset.lat_grid
		self.mlons = self.basisset.lon_grid

	def get_potential(self,mlats=None,mlons=None):
		"""
		Input arguments don't do anything, just there for consistancy of interface
		"""
		return self.pot.reshape(mlats.shape)

class CS10ElectricPotential(object):
	"""
	The Cousins and Shepard 2010 Electric Potential Model
	we will use the Just SAM library to get access to this model
	"""
	def __init__(self,startdt,enddt,mlon_is_mlt_in_degrees=False):
		"""
			startdt, datetime.datetime
				The earliest time for which we would expect to get the conductance
				
				startdt and enddt set the time interval of solar wind data from 
				NASA OmniWeb for which we will precompute the averaged and shifted
				solar wind vars (By,Bz,Vsw,Dipole Tilt)
				(required by Ovation Prime) 

			enddt, datetime.datetime
				The latest time for which we would expect to get the potential 
		"""
		#
		self.startdt, self.enddt = startdt, enddt
		self.startjd, self.endjd = special_datetime.datetime2jd(self.startdt),special_datetime.datetime2jd(self.enddt)
		self.mlon_is_mlt_in_degrees = mlon_is_mlt_in_degrees

		#Load complete set of CS10 model coefficients in advance to speed up loop
		self.cs10Dir = os.path.join(just_sam.cs10.tables_dir,'cs10')
		self.cs10mod = just_sam.cs10.ModCoeff()

		#Preload time shifted and averaged OMNI data at 2 minute cadence
		#on call to calculate potential, we will look up closest driver value by Julian Date
		deltaT = 2
		deltaT_days = deltaT/60./24.
		self.jds = np.arange(self.startjd,self.endjd,deltaT_days)

		#Average over 45 min, ending 10 min prior to given time
		IMFdelay = 10
		IMFave   = 45
		(self.by, self.bz, self.vsw, self.tilt) = just_sam.sam._get_conds(startdt,enddt,deltaT,IMFave,IMFdelay)
		
		self.dt = None

	def get_potential(self,mlats,mlons):
		"""
		Take arrays (1D or 2D) of mlats and mlons
		and return array of electric potential of same shape
		"""
		
		#Figure out which hemisphere
		hemi = 'N' if np.nanmean(mlats)>0. else 'S'
		
		#Sanity check
		if self.dt is None:
			raise ValueError('You must set CS10ElectricPotential object variable dt to a datetime to calcluate the potential for!')
		if self.dt < self.startdt or self.dt > self.enddt:
			raise ValueError('Datetime in object variable dt is out of range!')

		#Calculate index of closest solar wind values BEFORE time self.dt
		thisjd = special_datetime.datetime2jd(self.dt)
		diff = self.jds-thisjd
		diff[diff>0.] = np.nan #Remove any possible times > desired time 
		nn = np.nanargmin(np.abs(diff))

		# Get CS10 model coeffs
		(sdcoeffs,sdlmin) = just_sam.cs10.calc_cs10_coeffs(hemi,self.by[nn],self.bz[nn],self.vsw[nn],self.tilt[nn],
														cs10mod=self.cs10mod,path=self.cs10Dir,silent=1)

		#Figure out what 0 mlon is in MLT (using same method as in just_sam, relying on davitpy AACGM (!!) )
		mltDef = just_sam.sam.aacgm.mltFromYmdhms(self.dt.year,self.dt.month,self.dt.day,
													self.dt.hour,self.dt.minute,self.dt.second,0.0) * 15.

		if not self.mlon_is_mlt_in_degrees:
			# mltDef is the rotation that needs to be applied, and lon is the AACGM longitude.
			# use modulo so new longitude is between 0 & 360
			mlt_lons = np.mod((mlons + mltDef), 360.)
		else:
			mlt_lons = mlons

		# Remove Model (for some reason it appears to use some non-standard magnetic longitude,
		#	which is just MLT in degrees)
		pot_sh = just_sam.basis.eval_pot_sh(mlats.flatten(),mlt_lons.flatten(),sdlmin,order=8)
		mod_pot = np.dot(pot_sh,sdcoeffs)
		mod_pot = mod_pot.reshape(mlats.shape)

		return mod_pot

class OvationPymeConductance(object):
	"""

	The Ovation Prime-based conductance model
	as used in:

	E.D.P Cousins, Tomoko Matsuo, and A.D. Richmond.
	'Mapping High-Latitude Ionospheric Electrodynamics with SuperDARN and AMPERE'
	JGR Space Physics, 2015
	
	"""
	def __init__(self,startdt,enddt,mlon_is_mlt_in_degrees=True):
		"""
			startdt, datetime.datetime
				The earliest time for which we would expect to get the conductance
				
				startdt and enddt set the time interval of solar wind data from 
				NASA OmniWeb that are available to calculate the Newell coupling function
				(required by Ovation Prime) 

			enddt, datetime.datetime
				The latest time for which we would expect to get the conductance 
		"""
		self.estimator = ovation_prime.ConductanceEstimator(startdt,enddt)
		self.mlon_is_mlt_in_degrees = mlon_is_mlt_in_degrees
		self.dt = None

	def get_conductance(self,mlat_grid,mlon_grid):
		"""
		Get hall and pedersen conductance at each location mlat(i),mlon(i)
		for true solar wind conditions for date and time dt 
		"""
		if self.dt is None:
			raise ValueError('Must set OvationPymeConductance object variable dt to something before calling get_conductance')
		
		hemi = 'N' if np.nanmean(mlat_grid.flatten())>0. else 'S'
		mlat_grid = np.abs(mlat_grid) # I think ovation only takes absolute latitudes

		#Convert magnetic longitude to MLT for ovation
		if not self.mlon_is_mlt_in_degrees:
			mlt_grid = amie_core.mlon2mlt(np.ones_like(mlon_grid.flatten())*special_datetime.datetime2jd(self.dt),
											mlon_grid.flatten(),self.dt.year)
			mlt_grid = mlt_grid.reshape(mlon_grid.shape)
		else:
			mlt_grid = mlon_grid/180.*12.
		
		ovation_mlatgrid,ovation_mltgrid,pedgrid,hallgrid = self.estimator.get_conductance(self.dt,hemi=hemi,auroral=True,solar=True)

		ped_interpolator = ovation_prime.LatLocaltimeInterpolator(ovation_mlatgrid,ovation_mltgrid,pedgrid)
		cond_ped_grid = ped_interpolator.interpolate(mlat_grid,mlt_grid)

		hall_interpolator = ovation_prime.LatLocaltimeInterpolator(ovation_mlatgrid,ovation_mltgrid,hallgrid)
		cond_hall_grid = hall_interpolator.interpolate(mlat_grid,mlt_grid)
		return cond_ped_grid, cond_hall_grid


		
import os, sys

def fix_fortan_padding(fname,maxwidth=12,max_cols=25):
	"""
	Fix the problem often encountered when reading 
	a Fortran-created formatted text file where
	there is no space between adjacent columns when a 
	negative sign is introduced
	"""
	fname_new = os.path.splitext(fname)[0]+'_fixed.csv'
	with open(fname,'r') as f:
		with open(fname_new,'w') as newf:
			for line in f.readlines():
				repad=[]
				for tok in line.split():
					irepad=0
					if len(list(tok)) >= maxwidth:
						#Check for negative sign
						for subtok in tok.strip().split('-'):
							if len(list(subtok))>1:
								if irepad<max_cols:
									repad.append('-'+subtok.strip())
									irepad+=1
					else:
						if irepad<max_cols:
							repad.append(tok.strip())
							irepad+=1
				newf.write(','.join(repad)+'\n')

	return fname_new

if __name__ == '__main__':
	fix_fortan_padding('/home/liamk/mirror/Projects/amiepy/amiepy/AmiePy/tables/test_amie_basis/ped_0788_0798.dat')
	fix_fortan_padding('/home/liamk/mirror/Projects/amiepy/amiepy/AmiePy/tables/test_amie_basis/hal_0788_0798.dat')
	
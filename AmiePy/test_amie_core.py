import amie_core
import pytest
import numpy as np
from numpy import testing as nptest

def test_regress_returns_like_matlab():
	"""
	Does the amie_core helper method regress return the
	same output as the equivalent MATLAB command
	"""
	#Tested using MATLAB
	expected_coefs = np.array([.95,.05])
	X = np.array([[0,0],[0,2],[1,1]])
	y = np.array([0,.1,1])
	coefs = amie_core.regress(X,y)
	nptest.assert_allclose(coefs,expected_coefs,atol=1e-14)

if __name__ == "__main__":
	#Run the tests
	pytest.main()
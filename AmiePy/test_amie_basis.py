import amie_basis, amie_core
import pytest
import numpy as np
import matplotlib.pyplot as pp
import os
from numpy import testing as nptest

#Where are the tables used in the tests (usually output from other AMIE implementations IDL/Matlab)
test_table_dir = os.path.join(amie_core.tables_dir,'test_amie_basis')

def test_compute_f_same_as_just_sam_fcmp():
	"""
	Does the amie_basis azimuthal part of AMIE functions 
	code return the same values for f_m as just_sam.basis._fcmp?
	"""
	expected_first_f_column = np.array([ -7.07106781e-01,   4.83689525e-01,  -2.45575608e-01,
         4.27435864e-15,   2.45575608e-01,  -4.83689525e-01,
         7.07106781e-01,  -9.09038955e-01,   1.08335044e+00,
        -1.22474487e+00,   1.32892605e+00,  -1.39272848e+00,
         1.00000000e+00,  -2.45575608e-01,   4.83689525e-01,
        -7.07106781e-01,   9.09038955e-01,  -1.08335044e+00,
         1.22474487e+00,  -1.32892605e+00,   1.39272848e+00,
        -1.41421356e+00,   1.39272848e+00,  -1.32892605e+00,
         1.22474487e+00])

	lons = np.linspace(-170.,170.,30.)
	basisset = amie_basis.BasisFunctionSet()
	f = basisset.compute_f(lons,mmx=12)
	nptest.assert_allclose(f[:,0],expected_first_f_column,atol=1e-14)

def test_potential_same_as_just_sam():
	"""
	Does amie_basis.BasisFunctionSet return same potential as just_sam.basis.eval_amie_pot?
	"""
	#First 6 basis functions for a single location
	expected_pot = np.array([ -5.93852379e-03,  -3.01748061e-02,  -6.55147131e-02,
         -6.77200084e-02,   1.58382391e-02,   7.27993422e-02])

	lats,lons = np.array([70.]),np.array([100.])
	basisset = amie_basis.BasisFunctionSet()
	pot = basisset.get_potential(lats,lons)

	nptest.assert_allclose(pot[0,:6],expected_pot,atol=1e-14)


def test_calc_potcoeff_to_groundmagcoeff_returns_same_as_fortran():
	"""
	Does amie_basis.BasisFunctionSet calculate zfma,vfma the same
	as get_gmagbasis.f90?
	""" 
	#Load conductance from precomputed data files
	ped_file = os.path.join(test_table_dir,'grid_ped_783')
	hall_file = os.path.join(test_table_dir,'grid_hal_783')
	
	basisset = amie_basis.BasisFunctionSet()
	ithtrns = basisset.qmeta['ithtrns']

	cond_ped = np.zeros((basisset.qmeta['ithmx'],36))
	cond_hall = np.zeros((basisset.qmeta['ithmx'],36))
	cond_ped.fill(.1)
	cond_hall.fill(.1)
	
	cond_ped_az = np.transpose(np.loadtxt(ped_file,dtype=np.float64)) # Auroral Zone conductance
	cond_hall_az = np.transpose(np.loadtxt(hall_file,dtype=np.float64)) # Auroral Zone conductance
	
	#neglect first column and row because of off by one in FTRAN
	cond_ped[:ithtrns,:] = cond_ped_az[1:,1:]
	cond_hall[:ithtrns,:] = cond_hall_az[1:,1:]

	#Load desired test results from output data files of get_gmagbasis.f90
	z_file = os.path.join(test_table_dir,'zfma_0778_0788.dat')
	v_file = os.path.join(test_table_dir,'vfma_0778_0788.dat')
	t_file = os.path.join(test_table_dir,'tfma_0778_0788.dat')
	p_file = os.path.join(test_table_dir,'psfma_0778_0788.dat')
	zfma_ftran,vfma_ftran = np.loadtxt(z_file,dtype=np.float64),np.loadtxt(v_file,dtype=np.float64)
	tfma_ftran,psfma_ftran = np.loadtxt(t_file,dtype=np.float64),np.loadtxt(p_file,dtype=np.float64)
	
	#Generate zfma and vfma from AmiePy
	zfma,vfma,tfma,psfma = basisset.calc_potcoeff_to_groundmagcoeff(cond_hall=cond_hall,cond_ped=cond_ped)

	#f1 = pp.figure()
	#f2 = pp.figure()

	#a1,a2 = f1.add_subplot(111),f2.add_subplot(111)
	#cmin,cmax = np.nanmin(vfma_ftran),np.nanmax(vfma_ftran)
	#map1 = a1.imshow(psfma_ftran)
	#map2 = a2.imshow(psfma)
	#f1.colorbar(map1)
	#f2.colorbar(map2)
	#a1.set_title('Fortran')
	#a2.set_title('Python')
	#pp.show()

	#Pick a small selection to test
	delta = zfma_ftran[30:40,30:40]-zfma[30:40,30:40]
	nptest.assert_allclose(vfma[:30,:30],vfma_ftran[:30,:30],atol=1e-8)

if __name__ == "__main__":
	#Run the tests
	pytest.main()
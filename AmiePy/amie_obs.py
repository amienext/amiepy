from apexpy import apex_converter
import h5py
from geospacepy import special_datetime, satplottools, astrodynamics2
import numpy as np
import matplotlib.pyplot as pp
import datetime, os 
import amie_basis, amie_core

class ObservationApexLocations(object):
	"""
	A class representing a set of locations of observations
	"""
	def __init__(self,dts,glats,glons,alts,igrf_epoch,hr=110.,mlon_is_mlt_in_degrees=True):
		self.apexconv = apex_converter(epoch=igrf_epoch)
		self.mlon_is_mlt_in_degrees = mlon_is_mlt_in_degrees
		self.hr = hr # Apex reference height (110 km by default)
		self.Re = 6371.2 #Earth radius (mean)
		self.dts = dts
		self.glats = glats
		self.glons = glons
		self.alts = alts
		self.compute_apex()
		
	def compute_apex(self):
		"""
		Convert the observation locations in geocentric to apex
		"""
		self.lats,self.lons,self.qdlats = self.apexconv.geo2apex(self.glats,self.glons,self.alts)
		if self.dts[-1].year != self.dts[0].year:
			#we have multiple years
			years = np.array([dt.year for dt in self.dts])
		else:
			#We have only one year in the datetimes
			years = np.ones_like(self.glats)*self.dts[0].year

		doys,uts = special_datetime.datetimearr2doy(self.dts),special_datetime.datetimearr2sod(self.dts)		
		self.mlts = self.apexconv.alon2mlt(self.lons,years,doys,uts)

		#I think AMIE basis functions are specified in terms of 'longitude' being
		#MLT in degrees and not actually apex longitude
		if self.mlon_is_mlt_in_degrees:
			self.lons = self.mlts/12*180.

		#Copy apex basis vectors and pseudo-inclination to class
		self.d1,self.d2,self.d3 = self.apexconv.lastrun['d1'],self.apexconv.lastrun['d2'],self.apexconv.lastrun['d3']
		self.e1,self.e2,self.e3 = self.apexconv.lastrun['e1'],self.apexconv.lastrun['e2'],self.apexconv.lastrun['e3']
		self.f1,self.f2 = self.apexconv.lastrun['f1'],self.apexconv.lastrun['f2']
		self.sinIm = self.apexconv.lastrun['sim']

class AmieObservations(object):
	"""
	
	Prototype for observations classes

	Currently needs following functions

	lats,lons = observations.get_locations()
	y,y_var = observations.get_obs_and_errs()
	H = observations.get_obs_to_basis_matrix() 

	"""
	def __init__(self,lats,lons):
		pass

class Ampere(object):
	"""
	A subclass of observations for AMPERE magnetometer data
	
	Data in each NetCDF file
	['time', #Units of hours of day
 	'pseudo_sv_quality',
 	'pos_eci',
 	'b_eci',
 	'plane_num',
 	'pseudo_sv_num',
	'data_splice'
	]

	"""
	def __init__(self,ncdf_dt,ampere_ncdf,basisset=None):
		cache=True
		redo_cache=False
		
		self.basisset = amie_basis.BasisFunctionSet() if basisset is None else basisset

		self.ampere_ncdf = ampere_ncdf
		self.ampere_apex_h5 = os.path.splitext(self.ampere_ncdf)[0]+'_apex.h5'
		
		if redo_cache and os.path.exists(self.ampere_apex_h5):
			os.remove(self.ampere_apex_h5)

		self.ncdf_dt = ncdf_dt
		
		self.h5 = h5py.File(self.ampere_ncdf) #Open ampereCDF as an hdf5 dataset

		#There are 'spliced' fake observations that are interpolations from
		#leading trailing satellites. In this case data_splice==0
		splice = self.h5['data_splice'][:]
		real_obs = splice==1
		self.real_obs = real_obs

		#Get the quality factor for assigning uncertainty
		qf = self.h5['pseudo_sv_quality'][:]
		pvn = self.h5['pseudo_sv_num'][:]

		self.err = qf[self.real_obs]*200.
		self.pvn = pvn[self.real_obs]
		
		#Convert time units from hours to julian date and then to datetime
		self.hod = self.h5['time'][:][real_obs]
		#self.jds = self.hod/24.+special_datetime.datetime2jd(ncdf_dt)
		self.dts = special_datetime.sodarr2datetime(self.hod*3600.,ncdf_dt.year,ncdf_dt.month,ncdf_dt.day).flatten()
		self.jds = special_datetime.datetimearr2jd(self.dts).flatten()
		igrf_epoch = ncdf_dt.year+ncdf_dt.month/12.

		if cache and os.path.exists(self.ampere_apex_h5):
			print "Loading Ampere Apex and Geocentric Data From %s" % (self.ampere_apex_h5)
			#Load geo and apex from cache file
			with h5py.File(self.ampere_apex_h5) as h5apx:
				self.lats,self.lons,self.rs = h5apx['GeoLatitude'][:],h5apx['GeoLongitude'][:],h5apx['GeoRadius'][:]
				self.b_enu = h5apx['B_ENU'][:]
				self.mlats,self.mlons = h5apx['ApexLatitude'][:],h5apx['MLTLongitude'][:]
				self.dBd1, self.dBd2, self.dBd3 = h5apx['dBd1'][:],h5apx['dBd2'][:],h5apx['dBd3'][:]

			#Have to do this anyway because need factors
			self.apexobs = ObservationApexLocations(self.dts,self.lats,self.lons,self.rs-6371.2,igrf_epoch)

		else:
			#Calculate ECI to ECEF to GEO to APEX
			self.lats,self.lons,self.rs,self.b_enu = self.get_ampere_geo()

			#self.b_enu[:,2]=0.
			#along,across = self.calc_along_across()
			
			#for i in range(3):
			#	self.b_enu[:,i] = self.b_enu[:,i]*along[:,i]

			self.apexobs = ObservationApexLocations(self.dts,self.lats,self.lons,self.rs-6371.2,igrf_epoch)

			# as in all AMIE things, mlons is MLT in degrees
			self.mlats,self.mlons = self.apexobs.lats,self.apexobs.mlts/12*180. 

			#Compute observations in Apex
			self.dBd1,self.dBd2,self.dBd3 = np.zeros((self.b_enu.shape[0],)),np.zeros((self.b_enu.shape[0],)),np.zeros((self.b_enu.shape[0],))
			for i in range(self.b_enu.shape[0]):
				self.dBd1[i] = np.dot(self.apexobs.e1[i,:],self.b_enu[i,:])
				self.dBd2[i] = np.dot(self.apexobs.e2[i,:],self.b_enu[i,:])
				self.dBd3[i] = np.dot(self.apexobs.e3[i,:],self.b_enu[i,:])
			
			#self.validate_on_532()

			self.dBeast = self.b_enu[:,0]
			self.dBeq = self.b_enu[:,1]*np.sign(self.lats)*-1.

		if cache and not os.path.exists(self.ampere_apex_h5):
			print "Caching Ampere Apex and Geocentric Data To %s" % (self.ampere_apex_h5)
			#Create cache file
			with h5py.File(self.ampere_apex_h5) as h5apx:
				h5apx['JulianDate'],h5apx['HourOfDay'] = self.jds,self.hod
				h5apx['GeoLatitude'],h5apx['GeoLongitude'],h5apx['GeoRadius'] = self.lats,self.lons,self.rs
				h5apx['B_ENU'] = self.b_enu
				h5apx['ApexLatitude'],h5apx['MLTLongitude'] = self.mlats,self.mlons
				h5apx['dBd1'],h5apx['dBd2'],h5apx['dBd3'] = self.dBd1,self.dBd2,self.dBd3


		#Set outside of init function to set window of data to retrieve
		self.startdt = None
		self.enddt = None
		self.hemi = None
		self.minlat = 50.
		self.hold_for_cross_validation = 10 #Percent of data to hold for cross validation
		self.cross_validation_mask = None

	def validate_on_532(self):
		is532 = self.pvn==532
		in_zero_to_four = np.logical_and(self.hod>0,self.hod<=4./60)
		g = np.logical_and(is532,in_zero_to_four)
		f = pp.figure()
		a = pp.axes()
		a.plot(self.hod[g],self.dBd1[g],'b.')
		f.savefig('test_on_532.png')

	def calc_along_across(self):
		unique_pvns = np.unique(self.pvn)
		uts = special_datetime.datetimearr2sod(self.dts)
		along,across,up = np.zeros_like(self.b_enu),np.zeros_like(self.b_enu),np.zeros_like(self.b_enu) 
		for pvn in unique_pvns.flatten().tolist():
			in_pvn = self.pvn == pvn
			pvn_lat,pvn_lon = self.lats[in_pvn],self.lons[in_pvn]
			along[in_pvn,:],across[in_pvn,:],up[in_pvn,:] = satplottools.sathat(uts[in_pvn],np.column_stack((pvn_lat,pvn_lon)))
		return along, across
		
	def get_ampere_geo(self,dt=None,r_eci=None,b_eci=None):

		if dt is None:
			jd = self.jds
		else:
			jd = special_datetime.datetimearr2jd(dt)

		if r_eci is None or b_eci is None:
			real_obs = self.real_obs #Selection for unspliced observations
			
			#Load in the locations and data (need [:] to convert from HDF dataset object to np.ndarray)
			r_eci,b_eci = self.h5['pos_eci'][:][real_obs,:],self.h5['b_eci'][:][real_obs,:]
		
		npts = r_eci.shape[0] #Number of AMPERE datapoints

		#Rotate about the earth's nomnial rotation axis by the GST to
		#go from inertial coordinates to earth centered earth fixed
		b_enu = np.zeros_like(b_eci)
		lats,lons,rs = np.zeros((npts,)),np.zeros((npts,)),np.zeros((npts,))
		print "Rotating %d AMPERE observations from ECI to ENU" % (npts)

		#Iterate because astrodynamics2 functions aren't vectorized
		for i in range(r_eci.shape[0]):

			#Calculate the greenwich hour angle in degrees
			gst = astrodynamics2.jd2gst(jd[i],deg=True)
			
			#Rotates spacecraft position to ECEF
			r_ecef = astrodynamics2.rot3(gst,r_eci[i,:],deg=True)

			#Convert from cartesian ECEF to lat lon r 
			x,y,z = r_ecef[0]/1.0e3,r_ecef[1]/1.0e3,r_ecef[2]/1.0e3
			r = np.sqrt(x**2+y**2+z**2)
			theta = np.arctan2(np.sqrt(x**2+y**2),z)
			phi = np.arctan2(y,x)
			
			#Store geographic position
			lats[i],lons[i],rs[i] = 90.-np.degrees(theta),np.degrees(phi),r
			
			#Rotate observations to ECEF, then to East North Up
			b_ecef = astrodynamics2.rot3(gst,b_eci[i,:],deg=True)
			b_enu[i,:] = astrodynamics2.ecef2enu(b_ecef,lats[i],lons[i])
			#b_enu[i,:]=b_ecef

			#Status message
			if np.mod(i,1000)==0:
				print "Rotation %d/%d" % (i,r_eci.shape[0])
				#print "b_ecef was %s, b_enu was %s" % (str(b_ecef),str(b_enu[i,:]))
				#print "R was %f" % (r)

		return lats,lons,rs,b_enu

	def get_time_mask(self):
		if self.startdt is None or self.enddt is None:
			raise ValueError('Must set Ampere.startdt and Ampere.enddt to desired time range!')
		startjd = special_datetime.datetime2jd(self.startdt)
		endjd = special_datetime.datetime2jd(self.enddt)
		if startjd<self.jds[0] or endjd>self.jds[-1]:
			raise ValueError('Time range %s-%s is out of range for data in AMPERE object (%s-%s)' % (self.startdt.strftime('%c'),
																								self.enddt.strftime('%c'),
																								self.dts[0].strftime('%c'),
																								self.dts[-1].strftime('%c')))
		intime = np.logical_and(self.jds.flatten()>=startjd,self.jds.flatten()<endjd)
		print "%d values between %s and %s" % (np.count_nonzero(intime),
											   self.startdt.strftime('%c'),
											   self.enddt.strftime('%c'))

		return intime

	def get_hemi_mask(self):
		if self.hemi is None or self.hemi not in ['N','S']:
			raise ValueError('Must set Ampere.hemi to N or S!')
		#Since self.mlats has shape (n_records,n_stations) we will just take first row since mlat of mags does not
		#change with time
		inhemi = self.mlats.flatten() > self.minlat if self.hemi is 'N' else self.mlats.flatten() < -1*self.minlat
		print "%d values in %s hemisphere" % (np.count_nonzero(inhemi),self.hemi)
		return inhemi

	def get_validation_mask(self):
		"""
		Sets aside a percentage of the data chosen randomly as a cross validation set
		"""
		if self.cross_validation_mask is None:
			self.cross_validation_mask = np.zeros_like(self.jds,dtype=bool)
			if self.hold_for_cross_validation is not None and self.hold_for_cross_validation > 0.:
				per = self.hold_for_cross_validation/100.
				n_obs = self.jds.shape[0]
				n_xval = np.floor(per*n_obs)
				i_xval = np.random.randint(n_obs,size=(n_xval,))
				self.cross_validation_mask[i_xval]=True
		return self.cross_validation_mask

	def get_ingest_data(self,obs_err=None,validation_set=False):
		"""

		Formats the observations in the correct way so that they can be ingested into AMIE


		"""
		intime = self.get_time_mask()
		inhemi = self.get_hemi_mask()
		inval = self.get_validation_mask()
		
		#Mask to remove any points with no data for the entire inteval
		g = np.logical_and(inhemi,intime)
		g = np.logical_and(g,inval) if validation_set else np.logical_and(g,np.logical_not(inval))
		g = np.logical_and(g,np.logical_not(np.isnan(self.dBd1)))
		g = np.logical_and(g,np.logical_not(np.isnan(self.dBd2)))

		dBd1 = self.dBd1[g].flatten()
		dBd2 = self.dBd2[g].flatten()
		err = self.err[g].flatten()
		
		#Filter the locations so the number match
		lats_intime = np.concatenate([self.mlats[g].flatten(),self.mlats[g].flatten()])
		lons_intime = np.concatenate([self.mlons[g].flatten(),self.mlons[g].flatten()])
		
		#Format the vector measurements as a 1-D observation vector,
		#with each component as a seperate observation
		y = np.concatenate([dBd1,dBd2])
		print("Mean dBd1: %f, Mean dBd2: %f" % (np.nanmean(np.abs(dBd1)),np.nanmean(np.abs(dBd2))))
		
		#Format the error/variance vector similarly, this will be the diagonal of C_b / R, representativeness/observation
		#covariance
		if obs_err is None:
			#Use quality factor * 200nT
			y_var = np.concatenate([err**2,err**2])
		else:
			y_var = np.ones_like(y)*obs_err**2
		
		#print str(y.shape),y

		H_dBd1,H_dBd2 = self.basisset.get_magnetic_perturbations(self.apexobs,obs_subset=g)
		
		#Obs to basis is also called forward operator H
		obs_to_basis = np.concatenate([H_dBd1,
								   H_dBd2],axis=0)

		return lats_intime,lons_intime,y,y_var,obs_to_basis

	def plot_vectors(self,ax,validation_set=False):
		"""
		Plot the magnetometer observations at their locations
		on a polar plot
		"""
		intime = self.get_time_mask()
		inhemi = self.get_hemi_mask()
		inval = self.get_validation_mask()
		
		#Mask to remove any stations with no data for the entire inteval
		g = np.logical_and(inhemi,intime)
		g = np.logical_and(g,inval) if validation_set else np.logical_and(g,np.logical_not(inval))
		g = np.logical_and(g,np.logical_not(np.isnan(self.dBd1)))
		g = np.logical_and(g,np.logical_not(np.isnan(self.dBd2)))

		#Filter the locations so the number match
		lats_intime,lons_intime = self.mlats[g].flatten(),self.mlons[g].flatten()

		X,Y = satplottools.latlon2cart(lats_intime,lons_intime,self.hemi)

		#Calculate arrow directions for vector plot
		phi = (lons_intime+90.)/180.*np.pi #+90 for zero of phi at -Y instead of +X as usual since lons are MLT

		#B_east = self.dBd1[g].flatten()
		#B_eq = self.dBd2[g].flatten()
		B_east = self.dBd1[g].flatten()
		B_eq = self.dBd2[g].flatten()

		eq_hat_x,eq_hat_y = np.cos(phi),np.sin(phi)
		east_hat_x,east_hat_y = np.sin(phi),-1.*np.cos(phi)

		#Convert vectors to cartesian from top-down polar
		B_x = B_east*east_hat_x + B_eq*eq_hat_x
		B_y = B_east*east_hat_y + B_eq*eq_hat_y
		#vel_x = -1*vel_x
		#vel_y = -1*vel_y
		g = np.logical_and(np.logical_not(np.isnan(B_x)),np.logical_not(np.isnan(B_y)))

		Q1 = ax.quiver(X[g], Y[g], B_x[g], B_y[g], color='black', 
			angles='xy', scale_units='xy', scale=500./10.,alpha=.5, headwidth=2., headlength=1.,width=.004)
		ax.quiverkey(Q1,.75,0.05,500.,'500 nT')

	def plot_expected_vectors(self,ax,x,H):
		"""

			x - the assimilation or model result weights of AMIE basis functions
			H - the matrix which transforms observations into amie basis functions

		"""
		intime = self.get_time_mask()
		inhemi = self.get_hemi_mask()

		#Mask to remove any stations with no data for the entire inteval
		g = np.logical_and(inhemi,intime)
		g = np.logical_and(g,np.logical_not(np.isnan(self.dBd1)))
		g = np.logical_and(g,np.logical_not(np.isnan(self.dBd2)))

		#Filter the locations so the number match
		lats_intime,lons_intime = self.mlats[g].flatten(),self.mlons[g].flatten()

		n_obs = len(lats_intime)

		B_east = np.dot(H[:n_obs,:],x)
		B_eq = np.dot(H[n_obs:2*n_obs,:],x)

		X,Y = satplottools.latlon2cart(lats_intime,lons_intime,self.hemi)

		#Calculate arrow directions for vector plot
		phi = lons_intime/180.*np.pi
		
		eq_hat_x,eq_hat_y = np.cos(phi),np.sin(phi)
		east_hat_x,east_hat_y = -1.*np.sin(phi),np.cos(phi)
		#Convert vectors to cartesian from top-down polar
		B_x = B_east*east_hat_x + B_eq*eq_hat_x
		B_y = B_east*east_hat_y + B_eq*eq_hat_y

		g = np.logical_and(np.logical_not(np.isnan(B_x)),np.logical_not(np.isnan(B_y)))

		Q1 = ax.quiver(X[g], Y[g], B_x[g], B_y[g], color='black', 
			angles='xy', scale_units='xy', scale=500./10.,alpha=.8, headwidth=2., headlength=1.,width=.004)
		ax.quiverkey(Q1,.75,0.05,500.,'500 nT')

		max_expected = np.nanmax(np.sqrt(B_east**2+B_eq**2))
		print "Maximum expected vector %f nT" % (max_expected)


class DMSP(object):
	"""
	A subclass of observations for DMSP in-situ magnetometer data
	"""
	def __init__(self):
		pass

class SuperMAG(object):
	"""
	A subclass of observations for SuperMAG ground magnetometer data
	"""
	def __init__(self,basisset=None,supermag_file=None,mlon_is_mlt_in_degrees=False):
		if basisset is None:
			basisset = amie_basis.BasisFunctionSet()
		self.basisset = basisset
		
		#Assume that longitude is MLT in degrees
		#Apparently this is what is done in the original AMIE...who knew
		self.mlon_is_mlt_in_degrees = mlon_is_mlt_in_degrees

		#We can manually load a SuperMAG ASCII file downloaded from website
		if supermag_file is not None:
			dts,jds,mlats,mlts,Bn,Be,Bz,station_cols = self.parse_ascii(supermag_file)

			self.station_cols = station_cols
			self.record_dts = np.array(dts)
			self.record_jds = special_datetime.datetimearr2jd(self.record_dts)
			#These come out as n_records x n_stations, so we will probably want them flattened
			#except dts, which only has one value per record and is a list, not an array
			
			self.jds,self.mlats,self.mlts = jds,mlats,mlts
			self.Bn,self.Be,self.Bz = Bn,Be,Bz
			
			if not self.mlon_is_mlt_in_degrees:
				self.mlons = amie_core.mlt2mlon(self.jds.flatten(),self.mlts.flatten(),self.record_dts[0].year)
				self.mlons.reshape(self.mlts.shape)
			else:
				self.mlons = self.mlts/12.*180. #Convert MLT to degrees

		print self.record_jds.shape
		print self.jds.shape
			
		#Set outside of init function to set window of data to retrieve
		self.startdt = None
		self.enddt = None
		self.hemi = None
		self.hold_for_cross_validation = 20
		self.cross_validation_mask = None
		self.minlat = 50.

	def get_time_mask(self):
		startjd = special_datetime.datetime2jd(self.startdt)
		endjd = special_datetime.datetime2jd(self.enddt)
		if startjd<self.record_jds[0] or endjd>self.record_jds[-1]:
			raise ValueError('Time range %s-%s is out of range for data in SuperMAG object (%s-%s)' % (self.startdt.strftime('%c'),
																								self.enddt.strftime('%c'),
																								self.record_dts[0].strftime('%c'),
																								self.record_dts[-1].strftime('%c')))
		return np.logical_and(self.record_jds.flatten()>=startjd,self.record_jds.flatten()<endjd)

	def get_hemi_mask(self):
		if self.hemi is None or self.hemi not in ['N','S']:
			raise ValueError('Must set SuperMAG.hemi to N or S!')
		#Since self.mlats has shape (n_records,n_stations) we will just take first row since mlat of mags does not
		#change with time
		inhemi = self.mlats[0,:].flatten() > self.minlat if self.hemi is 'N' else self.mlats[0,:].flatten() < -1*self.minlat
		return inhemi

	def get_validation_mask(self):
		"""
		Sets aside a percentage of the data chosen randomly as a cross validation set
		"""
		if self.cross_validation_mask is None:
			self.cross_validation_mask = np.zeros_like(self.jds[0,:],dtype=bool) #Selecting stations
			if self.hold_for_cross_validation is not None and self.hold_for_cross_validation > 0.:
				per = self.hold_for_cross_validation/100.
				n_obs = self.jds.shape[1] #Select stations which will be excluded
				n_xval = np.floor(per*n_obs)
				i_xval = np.random.randint(n_obs,size=(n_xval,))
				self.cross_validation_mask[i_xval]=True
		return self.cross_validation_mask

	def get_ingest_data(self,obs_err=30.,validation_set=False):
		"""

		Formats the observations in the correct way so that they can be ingested into AMIE

		Currently using a fixed observation/representativeness error of 3000 V 
		for all ground mags (from MATLAB AMIE)
		is this appropriate??

		"""
		
		intime = self.get_time_mask()
		
		inhemi = self.get_hemi_mask()
		invalidation = self.get_validation_mask() #Some stations will be selected for validation (or not)
		inhemi = np.logical_and(inhemi, invalidation if validation_set else np.logical_not(invalidation))

		first_intime = np.flatnonzero(intime)[0]
		lats_intime,lons_intime = self.mlats[first_intime,:].flatten(),self.mlons[first_intime,:].flatten()

		#Find the mean of each component for each station across the time window
		Bn = np.nanmean(self.Bn[intime,:],axis=0).flatten()
		Be = np.nanmean(self.Be[intime,:],axis=0).flatten()
		Bz = np.nanmean(self.Bz[intime,:],axis=0).flatten()
		
		#Mask to remove any stations with no data for the entire inteval
		g = inhemi
		g = np.logical_and(g,np.logical_not(np.isnan(Bn)))
		g = np.logical_and(g,np.logical_not(np.isnan(Be)))
		g = np.logical_and(g,np.logical_not(np.isnan(Bz)))

		#Filter the locations so the number match
		lats_intime, lons_intime = lats_intime[g],lons_intime[g]

		#Format the vector measurements as a 1-D observation vector,
		#with each component as a seperate observation
		y = np.concatenate([Bn[g],Be[g],Bz[g]])
		
		#Format the error/variance vector similarly, this will be the diagonal of C_b / R, representativeness/observation
		#covariance
		y_var = np.ones_like(y)*obs_err**2
		
		Bh_intime,Bd_intime,Bz_intime = self.basisset.get_ground_magnetic_perturbations(lats_intime,lons_intime)
		
		#Obs to basis is also called forward operator H
		obs_to_basis = np.concatenate([Bh_intime,
									   Bd_intime,
									   Bz_intime],axis=0)

		#Locations for each vector component
		lats = np.concatenate([lats_intime,lats_intime,lats_intime])
		lons = np.concatenate([lons_intime,lons_intime,lons_intime])
		
		return lats,lons,y,y_var,obs_to_basis

	def plot_vectors(self,ax,hemi):
		"""
		Plot the magnetometer observations at their locations
		on a polar plot
		"""
		intime = self.get_time_mask()

		first_intime = np.flatnonzero(intime)[0]
		lats_intime,lons_intime = self.mlats[first_intime,:].flatten(),self.mlons[first_intime,:].flatten()

		X,Y = satplottools.latlon2cart(lats_intime,lons_intime,hemi)

		#Calculate arrow directions for vector plot
		phi = lons_intime/180.*np.pi

		B_east = np.nanmean(self.Be[intime,:],axis=0).flatten()
		B_eq = np.nanmean(self.Bn[intime,:],axis=0).flatten() * (-1. if hemi is 'N' else 1.)

		eq_hat_x,eq_hat_y = np.cos(phi),np.sin(phi)
		east_hat_x,east_hat_y = np.sin(phi),-1.*np.cos(phi)
		#Convert vectors to cartesian from top-down polar
		B_x = B_east*east_hat_x + B_eq*eq_hat_x
		B_y = B_east*east_hat_y + B_eq*eq_hat_y
		#vel_x = -1*vel_x
		#vel_y = -1*vel_y
		g = np.logical_and(np.logical_not(np.isnan(B_x)),np.logical_not(np.isnan(B_y)))

		Q1 = ax.quiver(X[g], Y[g], B_x[g], B_y[g], color='black', 
			angles='xy', scale_units='xy', scale=200./10.,alpha=.5, headwidth=2., headlength=1.,width=.004)
		ax.quiverkey(Q1,.75,0.05,300.,'200 nT')

	def plot_expected_vectors(self,ax,x,H):
		"""

			x - the assimilation or model result weights of AMIE basis functions
			H - the matrix which transforms observations into amie basis functions

		"""
		intime = self.get_time_mask()
		inhemi = self.get_hemi_mask()
		first_intime = np.flatnonzero(intime)[0]
		lats_intime,lons_intime = self.mlats[first_intime,inhemi].flatten(),self.mlons[first_intime,inhemi].flatten()
		n_obs = len(lats_intime)


		B_eq = -1*np.dot(H[:n_obs,:],x)
		B_east = np.dot(H[n_obs:2*n_obs,:],x)

		X,Y = satplottools.latlon2cart(lats_intime,lons_intime,self.hemi)

		#Calculate arrow directions for vector plot
		phi = lons_intime/180.*np.pi
		
		eq_hat_x,eq_hat_y = np.cos(phi),np.sin(phi)
		east_hat_x,east_hat_y = -1.*np.sin(phi),np.cos(phi)
		#Convert vectors to cartesian from top-down polar
		B_x = B_east*east_hat_x + B_eq*eq_hat_x
		B_y = B_east*east_hat_y + B_eq*eq_hat_y

		g = np.logical_and(np.logical_not(np.isnan(B_x)),np.logical_not(np.isnan(B_y)))

		Q1 = ax.quiver(X[g], Y[g], B_x[g], B_y[g], color='black', 
			angles='xy', scale_units='xy', scale=200./10.,alpha=.8, headwidth=2., headlength=1.,width=.004)
		ax.quiverkey(Q1,.75,0.05,200.,'200 nT')

	def parse_ascii(self,fn,n_header_lines=71,max_stations=320,max_records=1440):
		"""
		Parse a SuperMAG ASCII data file into a grid of data
		Don't currently know how many stations to use for max. SuperMAG website says 'more than 300'.
		Records are 1 per minute
		"""
		station_cols = dict() #Dictionary with station IAGA identifiers as keys and column in which to store their data in values
		lnnum = 0 # Line number counter
		rcnum = 0 # Record number counter
		
		#Preallocate arrays
		dts = []
		jd = np.zeros((max_records,max_stations)) #Julian date
		Bn,Be,Bz = np.zeros((max_records,max_stations)),np.zeros((max_records,max_stations)),np.zeros((max_records,max_stations))
		mlat,mlt,mlon = np.zeros((max_records,max_stations)),np.zeros((max_records,max_stations)),np.zeros((max_records,max_stations))
		Bn.fill(np.nan)
		Be.fill(np.nan)
		Bz.fill(np.nan)
		mlat.fill(np.nan)
		mlt.fill(np.nan)
		mlon.fill(np.nan)
		record_info = {'B_N':{'RecCol':0,'Arr':Bn},
						'B_E':{'RecCol':1,'Arr':Be},
						'B_Z':{'RecCol':2,'Arr':Bz},
						'MLT':{'RecCol':3,'Arr':mlt},
						'MLAT':{'RecCol':4,'Arr':mlat}}
		
		#Begin reading
		with open(fn,'r') as f:
			#Skip 71 lines of header
			while lnnum < n_header_lines:
				f.readline()
				lnnum+=1
			while True:
				#Read record description
				#year,month,day,hour,minute,second,n_stations
				recln = f.readline()
				if recln is None or not recln or recln.isspace():
					#end of file
					break
				else:
					#Empty strings are 'falsy' so ( if x ) is False if x == '' 
					rec_desc = [int(x) for x in recln.split() if (x and not x.isspace())]
					year,month,day,hour,minute,second,n_stations = rec_desc[:]
					rec_dt = datetime.datetime(year,month,day,hour,minute,second)
					rec_jd = special_datetime.datetime2jd(rec_dt)
					#Read data for stations for this time
					for i_ln in range(n_stations):
						ln = f.readline()
						lnlst = [x.strip() for x in ln.split() if (x and not x.isspace())]
						station_name = lnlst[0]
						recdata = [float(x) if x.strip() is not '999999' else np.nan for x in lnlst[1:] ]
						if station_name in station_cols:
							col = station_cols[station_name]
						else:
							col = len(station_cols.keys())
							station_cols[station_name] = col
						jd[rcnum,col] = rec_jd
						for var in record_info:
							vararray,rcol = record_info[var]['Arr'],record_info[var]['RecCol']
							if np.abs(recdata[rcol])>90000.:
								recdata[rcol]=np.nan
							vararray[rcnum,col]=recdata[rcol]

					dts.append(rec_dt)
					rcnum += 1
		
		nmags = len(station_cols.keys()) # Total number of columns which have one or more row of data
		return dts[:rcnum],jd[:rcnum,:nmags],mlat[:rcnum,:nmags],mlt[:rcnum,:nmags],Bn[:rcnum,:nmags],Be[:rcnum,:nmags],Bz[:rcnum,:nmags],station_cols
			
class SuperDARN(object):
	"""
	A subclass of observations for SuperDARN Line-of-Sight Ion convection data
	making use of functions within the Just SAM project to get the convection
	velocities 
	"""
	def __init__(self):
		pass